#include <iostream>
#include <vector>
#include <string>
#include "TFile.h"
#include "TTree.h"
#include "TPaveStats.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TF1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TROOT.h"

#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "TCanvas.h"
#include "RooAddPdf.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooConstVar.h"
#include "RooProduct.h"
#include "RooFormulaVar.h"
#include "RooFitResult.h"
#include "RooCustomizer.h"
using namespace RooFit ;


void carmelo_correction() 
{
  // max. number of photons
  const int Nphotons = 3;

  // max. number of missing dynodes
  const int Ndynodes = 9; 

  TString name, expr, title;
  RooArgList *pdfs = new RooArgList;
  RooArgList *coeffs = new RooArgList;
  RooArgList *model_pdfs = new RooArgList;
  RooArgList *model_coeffs = new RooArgList;
  RooAbsPdf *ph_pdf[Nphotons];
  RooFormulaVar *ph_coeff[Nphotons];

  // ADC counts
  RooRealVar adc("adc", "ADC counts", -1., 5.);
  adc.setBins(10000, "cache");

  // probability of missing a dynode
  RooRealVar Pmiss("Pmiss", "Prob. missing a dynode", 0.025);

  // gain suppression factor for one missing stage
  RooRealVar alpha("alpha", "Gain suppression factor", 1.25);
  
  // mean and sigma of noise
  RooRealVar mean_n("mean_n", "mean of noise gaussians", 0.); 
  RooRealVar sigma_n("sigma_n", "width of noise gaussians", 0.05); 
  
  // mean and sigma of single photon response
  RooRealVar mean_1ph("mean_1ph", "mean of single ph gaussian", 1.); 
  RooRealVar sigma_1ph("sigma_1ph", "width of single ph gaussian", 0.3); 

  // average number of incident photons on pixel
  RooRealVar navgph("navgph", "mean number of incident photons", 0.5);


  //==============
  // Build noise
  //==============
  RooAbsPdf *noise_pdf = new RooGaussian("G_noise", "Noise pdf", adc, mean_n, sigma_n); 
  RooFormulaVar *noise_coeff = new RooFormulaVar("NG_noise", "TMath::Poisson(0, navgph)", RooArgList(navgph));


  //========================================
  // Build modified single photon response
  //========================================
  for (int ndy = 0; ndy < Ndynodes + 1; ndy++) {
    // mean
    name.Form("mean_1ph_dy%i", ndy);
    expr.Form("mean_n + mean_1ph / TMath::Power(alpha, %i)", ndy);
    RooFormulaVar *mean = new RooFormulaVar(name, expr, RooArgList(mean_n, mean_1ph, alpha));

    // sigma
    name.Form("sigma_1ph_dy%i", ndy);
    expr.Form("sqrt(sigma_n*sigma_n + sigma_1ph*sigma_1ph / TMath::Power(alpha, %i))", ndy); // ci va la radice???
    RooFormulaVar *sigma = new RooFormulaVar(name, expr, RooArgList(sigma_n, sigma_1ph, alpha));

    // gaussian 
    name.Form("G_1ph_dy%i", ndy);
    title.Form("Gaussian for %i missing dynodes", ndy);
    RooAbsPdf *gauss = new RooGaussian(name, title, adc, *mean, *sigma); 

    // coefficient
    RooFormulaVar *coeff;
    name.Form("NG_1ph_dy%i", ndy); 
    if (ndy == 0) {
      expr.Form("(1. - %i*Pmiss)", Ndynodes); 
      coeff = new RooFormulaVar(name, expr, RooArgList(Pmiss));
    }
    else {
      expr.Form("Pmiss"); 
      coeff = new RooFormulaVar(name, expr, RooArgList(Pmiss));
    }

    pdfs->add( *gauss );
    coeffs->add( *coeff );
  }

  ph_pdf[0] = new RooAddPdf("1ph_pdf", "1 photon pdf", *pdfs, *coeffs);
  ph_coeff[0] = new RooFormulaVar("N_1ph", "TMath::Poisson(1, navgph)", RooArgList(navgph));


  //=================================
  // Build pdf for multiple photons
  //=================================
  for (int nph = 1; nph < Nphotons; nph++) {
    name.Form("%iph_pdf", nph+1);
    title.Form("%i photons pdf", nph+1);
    //ph_pdf[nph] = new RooNumConvPdf(name, title, adc, *(ph_pdf[nph-1]), *(ph_pdf[0])) ;
    ph_pdf[nph] = new RooFFTConvPdf(name, title, adc, *(ph_pdf[nph-1]), *(ph_pdf[0])) ;
    cout << nph + 1 << " photons pdf built!" << endl;
    //ph_pdf[nph]->Print("v");

    name.Form("N_ph%i", nph+1);
    expr.Form("TMath::Poisson(%i, navgph)", nph+1);
    ph_coeff[nph] = new RooFormulaVar(name, expr, RooArgList(navgph));
  }


  //====================
  // Build final model
  //====================
  model_pdfs->add( *noise_pdf );
  model_coeffs->add( *noise_coeff );
  for (int nph = 0; nph < Nphotons; nph++) {
    model_pdfs->add( *ph_pdf[nph] );
    model_coeffs->add( *ph_coeff[nph] );
  }
  RooAbsPdf *model = new RooAddPdf("model", "Spectrum model", *model_pdfs, *model_coeffs);


  //==================
  // Plot components
  //==================
  TCanvas* c = new TCanvas("c", "Carmelo's correction", 1000, 500);
  c->SetLogy();

  RooPlot* frame = adc.frame( Title(" ") );
  model->plotOn(frame, "", LineColor(kBlue), LineStyle(kSolid));

  for (int nph = 0; nph < Nphotons; nph++) {
    Double_t PoissCoef = TMath::Poisson(nph + 1, navgph.getVal());
    ph_pdf[nph]->plotOn(frame, "", LineColor(nph + 1), LineStyle(kSolid), Normalization(PoissCoef));
  }

  //frame->SetMinimum(1e-10);
  //frame->SetMaximum(1e-2);
  frame->Draw();
}

