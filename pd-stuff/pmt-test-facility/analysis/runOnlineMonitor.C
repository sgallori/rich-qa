// C++ inc
#include <iomanip>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

// ROOT inc
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TFile.h"
#include "TTree.h"
#include "TPaveStats.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TSpectrum.h"
#include "TChain.h"
#include "TAxis.h"
#include "TLatex.h"
#include "TBox.h"

#define NPMT 16
#define PMTDIM 8
#define BOXDIM 4
#define NPX PMTDIM*PMTDIM

// Map storing all monitoring info (per var, pmt, px)
std::map<TString, std::map<TString, std::vector<Float_t>> > moniVarMap;
std::map<TString, std::pair<Float_t, Float_t> > rangeMap;
std::map<TString, std::vector<TH1F*> > dataHistMap;


//////////////////////////////////////////
// Returns PMT position on the test box //
//////////////////////////////////////////
std::pair<Int_t, Int_t> getPosition(const TString pmt) 
{ 
  Int_t binx = 0, biny = 0;

  if (pmt.Contains("posA")) {
    binx = 0; 
    if (pmt.Contains("pmt2")) biny = 4*PMTDIM; 
    if (pmt.Contains("pmt1")) biny = 3*PMTDIM;
  }
  if (pmt.Contains("posB")) {
    binx = PMTDIM;
    if (pmt.Contains("pmt1")) biny = 4*PMTDIM;
    if (pmt.Contains("pmt2")) biny = 3*PMTDIM;
  }
  if (pmt.Contains("posC")) {
    binx = 2*PMTDIM;
    if (pmt.Contains("pmt2")) biny = 4*PMTDIM;
    if (pmt.Contains("pmt1")) biny = 3*PMTDIM;
  }
  if (pmt.Contains("posD")) {
    binx = 3*PMTDIM;
    if (pmt.Contains("pmt1")) biny = 4*PMTDIM;
    if (pmt.Contains("pmt2")) biny = 3*PMTDIM;
  }
  if (pmt.Contains("posE")) {
    binx = 0;
    if (pmt.Contains("pmt2")) biny = 2*PMTDIM;
    if (pmt.Contains("pmt1")) biny = PMTDIM;
  }
  if (pmt.Contains("posF")) {
    binx = PMTDIM;
    if (pmt.Contains("pmt1")) biny = 2*PMTDIM;
    if (pmt.Contains("pmt2")) biny = PMTDIM;
  }
  if (pmt.Contains("posG")) {
    binx = 2*PMTDIM;
    if (pmt.Contains("pmt2")) biny = 2*PMTDIM;
    if (pmt.Contains("pmt1")) biny = PMTDIM;
  }
  if (pmt.Contains("posH")) {
    binx = 3*PMTDIM;
    if (pmt.Contains("pmt1")) biny = 2*PMTDIM;
    if (pmt.Contains("pmt2")) biny = PMTDIM;
  }
  
  return std::make_pair(binx, biny);
} 


/////////////////////////////
// Plot text box on canvas //
/////////////////////////////
void plotTestBox() 
{  
  TBox *box = new TBox();
  box->SetFillStyle(0);
  box->SetLineWidth(2);

  // Draw test box
  for (int ipmt = 0; ipmt < NPMT; ipmt++) {
    Int_t x0 = (ipmt % BOXDIM) * PMTDIM; 
    Int_t y0 = (BOXDIM - ipmt/BOXDIM) * PMTDIM; 
    box->DrawBox(x0, y0 - PMTDIM, x0 + PMTDIM, y0);
  }
}
 

/////////////////////////////////////////////////////////////
// Plot monitoring variable for all pixels on 2D histogram //
/////////////////////////////////////////////////////////////
void plotMoni2DHistoPerPix(const TString datadir, const TString var) 
{
  gStyle->SetOptStat(0);

  TCanvas* canv = new TCanvas("canv_" + var, "Monitor for: " + var, 800, 800);
  TH2F *hmoni = new TH2F("hmoni_" + var, "Monitoring for: " + var, BOXDIM*PMTDIM, 0, BOXDIM*PMTDIM, BOXDIM*PMTDIM, 0, BOXDIM*PMTDIM);
  
  // Fill histogram
  for (auto &pmt : moniVarMap[var]) {
    Int_t binx0 = getPosition(pmt.first).first;
    Int_t biny0 = getPosition(pmt.first).second;
    
    for (int ipx = 0; ipx < NPX; ipx++) {
      hmoni->SetBinContent(binx0 + ipx%PMTDIM + 1, biny0 - ipx/PMTDIM, pmt.second[ipx]); 
    }
  }

  // Draw it
  hmoni->SetMinimum( rangeMap[var].first );
  hmoni->SetMaximum( rangeMap[var].second );
  hmoni->Draw("COLZ");
  plotTestBox();

  // Save canvas
  canv->SaveAs( datadir + "/" + var + ".pdf" );
} 


///////////////////////////////////////////////////////////
// Plot monitoring variable PMT-averaged on 2D histogram //
///////////////////////////////////////////////////////////
void plotMoni2DHistoPerPMT(const TString datadir, const TString var) 
{
  gStyle->SetOptStat(0);

  TCanvas* canv = new TCanvas("canv_avg" + var, "Monitor for: " + var + " (avg)", 800, 800);
  TH2F *hmoni = new TH2F("hmoni_avg_" + var, "Monitor for: " + var + " (avg)", BOXDIM, 0, BOXDIM*PMTDIM, BOXDIM, 0, BOXDIM*PMTDIM);
  TBox *box = new TBox();
  box->SetFillStyle(0);
  box->SetLineWidth(2);

  // Fill histogram
  for (auto &pmt : moniVarMap[var]) {
    Int_t binx0 = getPosition(pmt.first).first / PMTDIM;
    Int_t biny0 = getPosition(pmt.first).second / PMTDIM;

    // average value over PMT
    Float_t avg = std::accumulate(pmt.second.begin(), pmt.second.end(), 0.0) / pmt.second.size();
    for (int ipmt = 0; ipmt < BOXDIM; ipmt++) {
      hmoni->SetBinContent(binx0 + ipmt%BOXDIM + 1, biny0 - ipmt/BOXDIM, avg); 
    }
  }

  // Draw it
  hmoni->Draw("text");
  plotTestBox();
  
  // Save canvas
  canv->SaveAs( datadir + "/" + var + "_avg.pdf" );
}


//////////////////
// Plot spectra //
//////////////////
void plotSpectra(const TString datadir) 
{
  //for (auto pmt : dataHistoMap) {
  //  TCanvas* canv = new TCanvas("canv_" + pmt, "Monitor for: spectra of " + pmt.first, 800, 800);
  //} 
}


///////////////////////////
// Make monitoring plots //
///////////////////////////
void plotMoniHistograms(const TString datadir)
{
  plotSpectra(datadir);

  for (auto &var : moniVarMap) {
    plotMoni2DHistoPerPix(datadir, var.first);

    if (var.first == "MAROC gains") {
      plotMoni2DHistoPerPMT(datadir, var.first);
    }
  }
}


////////////////////////////////////////
// Write monitoring info on text file //
////////////////////////////////////////
void dumpMoniInfo(const TString outdir, const Int_t runNb) 
{
  TString fname = Form("%s/moni_dump_run0%i.dat", outdir.Data(), runNb);
  FILE *outfile = fopen(fname.Data(), "w");

  for (auto &var : moniVarMap) {
    fprintf(outfile, "%s \n", var.first.Data()); // write variable
    for (auto &pmt : var.second) { 
      fprintf(outfile, "%s \n", pmt.first.Data()); // write pmt
      size_t nPx = pmt.second.size();
      for (int ipx = 0; ipx < nPx; ipx++) {
        fprintf(outfile, "%i %f \n", ipx, pmt.second[ipx]); // write values for each pixel
      } 
    } 
  }
  fclose (outfile);
}


/////////////////////////////
// Process data of a board //
/////////////////////////////
void processBoard(const TString datadir, const Int_t runNb, const std::pair<const TString, TString> &feb)
{
  const int MAROC_init_gain = 128;

  UShort_t PMT_ADC[NPX]; 
  TSpectrum *s = new TSpectrum(50); // too much???
  TString pos = feb.first;
  TString addr = feb.second;

  // Open output file 
  TString file = Form("%s/feb-%s-000%i.root", datadir.Data(), addr.Data(), runNb);
  TFile *rootfile = TFile::Open(file, "read");   
  TTree *tree = (TTree*)rootfile->Get("mdfTree");
  Int_t nEvents = tree->GetEntries(); 

  // Process each PMT of the board
  for (int ipmt = 0; ipmt < 2; ipmt++) {
    TString pmt = Form("pos%s_feb%s_pmt%i", pos.Data(), addr.Data(), ipmt+1);
    for (auto var : moniVarMap) { var.second[pmt].reserve(NPX); } // reserve 
    dataHistMap[pmt].reserve(NPX);

    // Load ADC values
    TString branchName = Form("B01_PMT%i_ADC", ipmt+1);
    tree->SetBranchStatus("*", 0);
    tree->SetBranchStatus(branchName, 1);
    tree->SetBranchAddress(branchName, &PMT_ADC[0]);

    // Process each pixel
    for (int ipx = 0; ipx < NPX; ipx++) {
      Int_t nSignals = 0.;
      TString hname = Form("hdata_%s_%i", pmt.Data(), ipx);
      TH1F *hdata = new TH1F(hname, "Histogram with data", 270, -20, 250);;

      // 1) Loop over events
      for (Int_t iev = 0; iev < nEvents; iev++) {
        tree->GetEvent(iev); 
        hdata->Fill( Float_t(PMT_ADC[ipx]) ); 
        if (PMT_ADC[ipx] > 30.) nSignals++; 
      }//evts

      // 2) Run peak finding
      Double_t thr = 1. / hdata->GetBinContent(hdata->GetMaximumBin()); 	
      Int_t npeaks = s->Search(hdata, 3, "nodraw", thr); 
      Double_t *x_peaks = s->GetPositionX();
      Double_t *y_peaks = s->GetPositionY();
      Double_t ped = 0., gain = 0.;
      if (npeaks == 1) ped = x_peaks[0]; 
      if (npeaks >= 2) {
        ped = x_peaks[0];
        gain = x_peaks[1] - ped;
      }

      // Fill spectra
      dataHistMap[pmt].push_back(hdata);

      // Fill gains
      moniVarMap["Gain"][pmt].push_back(gain);
      
      // Fill ped pos
      moniVarMap["Pedestal pos"][pmt].push_back(ped);
      
      // Fill signal fraction
      Float_t frac = Float_t(nSignals) / nEvents;
      moniVarMap["Signal fraction"][pmt].push_back(frac);
      
      // MAROC gain for each px
      Float_t ref_gain = (256. - ped) / 3.; // 3p.e. peak at 256
      Float_t MAROC_gain = gain > 0. ? (ref_gain / gain) * MAROC_init_gain : 0.; 
      moniVarMap["MAROC gains"][pmt].push_back(MAROC_gain);

      //std::cout << "[INFO] pmt = " << pmt << ", ipx = " << ipx << ", gain = " << gain << std::endl;
    }//ipx
  }//ipmt
}


///////////////////////////
// Run online monitoring //
///////////////////////////
void runOnlineMonitor(const Int_t runNb, const TString datadir) 
{
  // Define your FEB addresses here!!!
  // Board naming convention:
  // |A|B| |C|D|
  // |E|F| |G|H|
  std::map<TString, TString> febMap; 
  febMap["A"] = "201B64D3";
  febMap["B"] = "5BD00500"; 
  febMap["C"] = "D36B699B";
  febMap["D"] = "7AF2432D"; 
  febMap["E"] = "1A065F35";
  febMap["F"] = "68664262";
  febMap["G"] = "29B01857"; 
  febMap["H"] = "DFD95A4A";

  // Define variables to be monitored 
  moniVarMap["Gain"] = {}; // gain
  rangeMap["Gain"] = std::make_pair(30., 150.);
  moniVarMap["Pedestal pos"] = {}; // pedestal position
  rangeMap["Pedestal pos"] = std::make_pair(15., 18.);
  moniVarMap["Signal fraction"] = {}; // Signal fraction
  rangeMap["Signal fraction"] = std::make_pair(0., 1e-3);
  moniVarMap["MAROC gains"] = {}; // MAROC gains
  rangeMap["MAROC gains"] = std::make_pair(60., 200.);

  // Start!
  time_t start, end;
  time( &start );

  // Process data
  for (auto &feb : febMap) {
    std::cout << "[INFO] Processing run " << runNb << ", board " << feb.second << std::endl;
    processBoard(datadir, runNb, feb);
  }
  
  // Make monitoring plots 
  plotMoniHistograms(datadir);

  // Dump monitoring info on text file
  dumpMoniInfo(datadir, runNb);

  // Stop!
  time( &end );
  int timing = difftime( end, start );

  // Print timing
  std::cout << std::endl;
  std::cout << "[INFO] Monitoring time: " << int(timing / 60) << " min" 
            << " and " << timing % 60 << " sec" << std::endl;
  std::cout << "[INFO] Done!" << std::endl;
}

