#include "Monitor.h" 


/////////////////////////////////
// Returns PMT position on map //
/////////////////////////////////
// Bins refer to top left corner of PMT, hence -1 for biny!
std::pair<Int_t, Int_t> Monitor::posOnBox(const TString pmt) 
{ 
  Int_t binx = 0, biny = 0;
  TString addr = m_pmtMap[pmt].first;
  Int_t pmtpos = m_pmtMap[pmt].second;
  TString febpos = m_febMap[addr];

  if (febpos == "A") {
    binx = 0; 
    biny = pmtpos == 2 ? 4*PMTDIM -1 : 3*PMTDIM -1;
  }
  if (febpos == "B") {
    binx = PMTDIM;
    biny = pmtpos == 1 ? 4*PMTDIM -1: 3*PMTDIM -1;
  }
  if (febpos == "C") {
    binx = 2*PMTDIM;
    biny = pmtpos == 2 ? 4*PMTDIM -1 : 3*PMTDIM -1;
  }
  if (febpos == "D") {
    binx = 3*PMTDIM;
    biny = pmtpos == 1 ? 4*PMTDIM -1 : 3*PMTDIM -1;
  }
  if (febpos == "E") {
    binx = 0;
    biny = pmtpos == 2 ? 2*PMTDIM -1 : PMTDIM -1;
  }
  if (febpos == "F") {
    binx = PMTDIM;
    biny = pmtpos == 1 ? 2*PMTDIM -1 : PMTDIM -1;
  }
  if (febpos == "G") {
    binx = 2*PMTDIM;
    biny = pmtpos == 2 ? 2*PMTDIM -1 : PMTDIM -1;
  }
  if (febpos == "H") {
    binx = 3*PMTDIM;
    biny = pmtpos == 1 ? 2*PMTDIM -1 : PMTDIM -1;
  }
  
  return {binx, biny};
} 


///////////////////
// Plot test-box //
///////////////////
void Monitor::plotBox(const TString opt, 
                      const bool draw_pmt_name) 
{  
  TBox *box = new TBox();
  box->SetFillStyle(0);
  box->SetLineWidth(1);

  TText *text = new TText();
  text->SetTextFont(42);

  // Book empty histogram
  Int_t nBins = BOXDIM*PMTDIM;
  TH2F *h = new TH2F("Box", "Test box", 
      nBins, 0, BOXDIM*PMTDIM, nBins, 0, BOXDIM*PMTDIM);
  h->Draw(opt);
                              
  // Draw test box
  for (int ipmt = 0; ipmt < NPMT; ipmt++) {
    Int_t x0 = (ipmt % BOXDIM) * PMTDIM; 
    Int_t y0 = (BOXDIM - ipmt/BOXDIM) * PMTDIM; 
    box->DrawBox(x0, y0 - PMTDIM, x0 + PMTDIM, y0);
  }

  // Draw pmt names on the centre if requested
  if (draw_pmt_name) {
    for (const auto &pmt : pmts()) {
      Int_t x0 = posOnBox(pmt).first + 1;
      Int_t y0 = posOnBox(pmt).second - PMTDIM / 2;
      text->DrawText(x0, y0, pmt);
    }
  }
}


//////////////////////////////////////////
// Plot variable tolerance on histogram //
//////////////////////////////////////////
void Monitor::plotTolerance(const TString var, TH1F* h)
{
  if (!m_tol.contains(var)) return;

  TLine *line = new TLine();
  line->SetLineColor(kRed);
  line->SetLineWidth(3);
  line->SetLineStyle(2);

  Float_t xmin = m_tol[var].min();
  Float_t xmax = m_tol[var].max();
  Float_t ymin = 0.;
  Float_t ymax = h->GetMaximum();
  if (xmin != -HUGE_VAL) line->DrawLine(xmin, ymin, xmin, ymax);
  if (xmax != HUGE_VAL) line->DrawLine(xmax, ymin, xmax, ymax);
}


//////////////////////////////////////
// Plot variable tolerance on graph //
//////////////////////////////////////
void Monitor::plotTolerance(const TString var, TGraphErrors* gr)
{
  if (!m_tol.contains(var)) return;

  TLine *line = new TLine();
  line->SetLineColor(kRed);
  line->SetLineWidth(3);
  line->SetLineStyle(2);

  Float_t ymin = m_tol[var].min();
  Float_t ymax = m_tol[var].max();
  Float_t xmin = gr->GetHistogram()->GetXaxis()->GetXmin();
  Float_t xmax = gr->GetHistogram()->GetXaxis()->GetXmax();
  if (ymin != -HUGE_VAL) line->DrawLine(xmin, ymin, xmax, ymin);
  if (ymax != HUGE_VAL) line->DrawLine(xmin, ymax, xmax, ymax);
}


///////////////////////////////////////////////////
// Plot variable on graph for all PMTs for a run //
///////////////////////////////////////////////////
TGraphErrors* Monitor::plotMoniGraph(const Run run, const TString var, 
                                     const TString yunit, 
                                     const Color_t col, const TString opt) 
{
  DataMap data = m_data[run][var];
  Data x, y, ex, ey; // errors not yet assigned!
  for (const auto &pmt : pmts()) y.add(data[pmt]);
  for (int i = 0; i < y.size(); i++) x.add(i+1);

  Range range = y.range();
  if (m_tol.contains(var)) range &= m_tol[var]; 

  TString name = var;
  TString title = Form("%s (run %i)", var.Data(), run.number);
  TString xtitle = "";
  TString ytitle = var + " " + yunit;
  TGraphErrors *gr = plotGraph(x, y, ex, ey, name, title, xtitle, ytitle, range, col, opt);

  // Plot pmt name on x-axis
  if (data.oneValued()) {
    TH1F *h = gr->GetHistogram(); 
    h->GetXaxis()->SetNdivisions(x.size());
    h->GetXaxis()->SetLabelSize(0.042);
    for (const auto val : x) {
      h->GetXaxis()->SetBinLabel(h->FindBin(val), pmts()[val-1]);
    } 
    h->GetXaxis()->LabelsOption("d");
  }

  plotTolerance(var, gr);

  return gr;
}


///////////////////////////////////////////////////
// Plot variable on graph for a PMT for all runs //
///////////////////////////////////////////////////
TGraphErrors* Monitor::plotMoniGraph(const TString var, const TString pmt, 
                                     const TString xunit, const TString yunit, 
                                     const Color_t col, const TString opt, 
        			     const Float_t xstart, const Float_t step)
{
  Data x, y, ex, ey; // errors not yet assigned!
  for (const auto &run : runs()) y.add(m_data[run][var][pmt]);
  for (int i = 0; i < y.size(); i++) x.add(xstart + i*step);

  Range range = y.range();
  if (m_tol.contains(var)) range &= m_tol[var]; 

  TString name = var + " " + pmt;
  TString title = var + " PMT " + pmt;
  TString xtitle = xunit;
  TString ytitle = var + " " + yunit;
  TGraphErrors *gr = plotGraph(x, y, ex, ey, name, title, xtitle, ytitle, range, col, opt);
  plotTolerance(var, gr);

  return gr;
}


//////////////////////////////////////////////
// Plot the map of variable on the test-box //
//////////////////////////////////////////////
TH2F* Monitor::plotMoniMap(const Run run, 
                           const TString var, 
                           const TString zunit,
			   const TString opt,
                           const bool draw_pmt_name)
{
  gStyle->SetOptStat(0);
  gStyle->SetPaintTextFormat("4.1f");
  TGaxis::SetMaxDigits(3); 

  gPad->SetTicks(1,1);
  gPad->SetLeftMargin(0.08);
  gPad->SetRightMargin(0.13);

  // Tell if var contains one value per PMT
  DataMap dmap = m_data[run][var];
  Bool_t oneValued = dmap.oneValued();

  // Book histogram
  Int_t nBins = BOXDIM*PMTDIM;
  if (oneValued) nBins /= PMTDIM; 
  TString name = ("Map " + var).ReplaceAll(" ", "_");
  TString title = Form("%s %s (run %i)", var.Data(), zunit.Data(), run.number);
  TH2F *h = new TH2F(name, title, nBins, 0, BOXDIM*PMTDIM, nBins, 0, BOXDIM*PMTDIM);
                              
  // Fill histogram
  for (const auto &pmt : pmts()) {
    const Data &data = dmap[pmt];
    Int_t binx0 = posOnBox(pmt).first; // pos on map
    Int_t biny0 = posOnBox(pmt).second;

    if (oneValued) { // one value per PMT
      h->SetBinContent(binx0/PMTDIM + 1, biny0/PMTDIM + 1, data[0]); 
    } 
    else {
      for (int i = 0; i < data.size(); i++) {
	h->SetBinContent(binx0 + i%PMTDIM + 1, biny0 - i/PMTDIM + 1, data[i]); 
      }
    }
  }

  // Draw it
  if (oneValued) {
    if (draw_pmt_name) h->SetBarOffset(0.2); // if pmt name it's drawn
    h->SetMarkerSize(3);
    h->SetMarkerColor(kRed);
    h->Draw(opt);
  }
  else {
    if (m_tol.contains(var)) {
      h->SetMinimum( m_tol[var].min() );
      h->SetMaximum( m_tol[var].max() );
    }
    h->GetZaxis()->SetTitle(zunit);
    h->Draw(opt);

    gPad->Update(); // adjust palette 
    TPaletteAxis *palette = (TPaletteAxis*)h->GetListOfFunctions()->FindObject("palette");
    palette->SetX1(h->GetXaxis()->GetXmax() + 0.3);
    palette->SetX2(h->GetXaxis()->GetXmax() + 1.5);
    h->Draw(opt);
  }
    
  plotBox("same", draw_pmt_name);

  return h;
} 


/////////////////////////////////////////
// Plot correlation graph wrt datsheet //
/////////////////////////////////////////
void Monitor::plotMoniCorrel(const Run run, 
                             const TString dsvar, const TString var, 
                             const TString xunit, const TString yunit)
{
  // Check datasheet
  if (!m_datasheet.contains(dsvar)) {
    std::cout << "[WARNING] Monitor::plotMoniCorrel(): " << dsvar 
      << " not in datasheet! Cannot plot!" << std::endl;
    return;

  }
  
  DataMap data = m_data[run][var];
  DataMap dsheet = m_datasheet[dsvar].submap(data.keys());
  Range range = data.range();
  if (m_tol.contains(var)) range &= m_tol[var];

  TString xtitle = "Datasheet " + dsvar + " " + xunit;
  TString ytitle = "Measured " + var + " " + yunit;

  // If one-valued dataset, draw correl. for all PMTs
  if (data.oneValued()) {
    TString name = "Datasheet " + dsvar + " vs " + var; 
    TString title = Form("Datasheet %s vs %s (run %i)", 
                          dsvar.Data(), var.Data(), run.number);
    TGraphErrors *gr = plotGraph(dsheet, data, {}, {}, name, title, xtitle, ytitle, range);
    plotTolerance(var, gr);
  }

  // Otherwise draw correl. for each pmt
  else {
    const Int_t npadx = BOXDIM, npady = BOXDIM;
    TVirtualPad *pad = gPad; // get current pad
    pad->Divide(npadx, npady);

    // Loop on pmts
    for (const auto &pmt : pmts()) {
      Int_t binx = posOnBox(pmt).first / PMTDIM + 1; 
      Int_t biny = posOnBox(pmt).second / PMTDIM + 1;
      int ipad = (BOXDIM - biny)*BOXDIM + binx;
      pad->cd(ipad); // draw each graph at right position on the box

      TString name = "Datasheet " + dsvar + " vs " + var + " PMT " + pmt; 
      TString title = Form("Datasheet %s vs %s PMT %s (run %i)", 
	                    dsvar.Data(), var.Data(), pmt.Data(), run.number);
      TGraphErrors *gr = plotGraph(dsheet[pmt], data[pmt], {}, {}, 
              	                   name, title, xtitle, ytitle, range, kBlue, "AP");
      plotTolerance(var, gr);
    }//pmt
  }
}


///////////////////////////////
// Plot monitoring histogram //
///////////////////////////////
void Monitor::plotMoniHisto(const Run run, 
                            const TString var, 
                            const TString xtitle,
			    Int_t nBins,
			    Range range,
                            const Color_t col,
                            const TString opt) 
{
  DataMap data = m_data[run][var];
  
  // If range not given, use data range and tolerance
  if (!range && m_tol.contains(var)) range = data.range() & m_tol[var]; 

  // If one-valued dataset, draw all pmts
  if (data.oneValued()) {
    TH1F *h = plot1DHisto(data, var, var, xtitle, nBins, range, col, opt);
    plotTolerance(var, h);
  }

  // Otherwise draw each pmt
  else {
    const Int_t npadx = BOXDIM, npady = BOXDIM;
    TVirtualPad *pad = gPad; // get current pad
    pad->Divide(npadx, npady);

    // Loop on pmts
    for (const auto &pmt : pmts()) {
      Int_t binx = posOnBox(pmt).first / PMTDIM + 1; 
      Int_t biny = posOnBox(pmt).second / PMTDIM + 1;
      int ipad = (BOXDIM - biny)*BOXDIM + binx;
      pad->cd(ipad); // draw each graph at right position on the box

      TH1F *h = plot1DHisto(data[pmt], var + " " + pmt, var + " " + pmt, xtitle, nBins, range, col, opt);
      plotTolerance(var, h);
    }//pmt
  }
}


////////////////////////////////////////////
// Plot analysis variable on 1D histogram //
////////////////////////////////////////////
void Monitor::plotAnaHisto(const TString var, 
                           const TString xtitle,
			   Int_t nBins,
			   Range range,
                           const Color_t col, 
                           const TString opt) 
{
  DataMap data = m_anaData[var];

  // If range not given, use data range and tolerance
  if (!range && m_tol.contains(var)) range = data.range() & m_tol[var]; 

  // If one-valued dataset, draw all pmts
  if (data.oneValued()) {
    TH1F *h = plot1DHisto(data, var, var, xtitle, nBins, range, col, opt);
    plotTolerance(var, h);
  }

  // Otherwise draw each pmt
  else {
    const Int_t npadx = BOXDIM, npady = BOXDIM;
    TVirtualPad *pad = gPad; // get current pad
    pad->Divide(npadx, npady);

    // Loop on pmts
    for (const auto &pmt : pmts()) {
      Int_t binx = posOnBox(pmt).first / PMTDIM + 1; 
      Int_t biny = posOnBox(pmt).second / PMTDIM + 1;
      int ipad = (BOXDIM - biny)*BOXDIM + binx;
      pad->cd(ipad); // draw each graph at right position on the box

      TH1F *h = plot1DHisto(data[pmt], var + " " + pmt, var + " " + pmt, 
	  xtitle, nBins, range, col, opt);
      plotTolerance(var, h);
    }//pmt
  }
}



