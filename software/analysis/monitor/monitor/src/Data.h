#ifndef DATA_H
#define DATA_H 1

// C++ inc
#include <iomanip>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <sstream>
#include <iterator>
#include <initializer_list>

// ROOT inc
#include "TROOT.h"

// Define run type
enum RunType { DarkRun, CalibRun, LEDRun, BadRun, GenericRun };

// Typedefs
typedef std::pair<TString, int> PmtPos; 


//=============================================
// Class describing a range bet. two numbers 
//=============================================
class Range
{
  public:
    Range() {};
    Range(const float min, const float max) { set(min, max); }

    // Set range
    void set(const float min, const float max) {
      if (min > max) {
	std:: cout << "[WARNING] Range::set(): illegal set of range: " 
	"min: "  << min << " > max: " << max << ". Cannot set!" << std::endl;
	return;
      }
      
      m_min = min;
      m_max = max;
    }
    void setMin(const float min) { this->set(min, this->max()); }
    void setMax(const float max) { this->set(this->min(), max); }

    // Returns min/max
    const float min() const { return m_min; }
    const float max() const { return m_max; }

    // Returns length of the range
    const float length() const { 
      float l = (m_max - m_min);
      if (l < 0.) 
	std:: cout << "[WARNING] Range::length(): length < 0.!" << std::endl;
      return l;
    }

    // Enlarge range by a common factor in left, right or both directions
    void enlarge(const float f) { this->enlargeLeft(f); this->enlargeRight(f); }
    void enlargeLeft(const float f) { this->setMin(this->min()*(1.-f));}
    void enlargeRight(const float f) { this->setMax(this->max()*(1.+f));}

    // Tells if it's an infinite range
    const bool isInf() const { return (std::abs(this->length()) == HUGE_VAL); }
    const bool isLowerInf() const { return (this->min() == -HUGE_VAL); }
    const bool isUpperInf() const { return (this->max() == +HUGE_VAL); }

    // <> overload (according to the length)
    const bool operator< (const Range &rhs) const {
      //return (this->min > rhs.min && this->max < rhs.max);
      return this->length() < rhs.length();
    }
    const bool operator> (const Range &rhs) const { return !(*this < rhs); }

    // &, &= overloads (combine two ranges)
    const Range operator& (const Range &rhs) const {
      Range r = *this;
      if (rhs.min() < r.min()) r.setMin(rhs.min());
      if (rhs.max() > r.max()) r.setMax(rhs.max());
      return r;
    }
    const Range operator&= (const Range &rhs) { 
      *this = *this & rhs; 
      return *this;
    }

    // ==, !=, ! overloads
    const bool operator! () const { return this->length() == 0.; } 
    const bool operator!= (const Range &rhs) const { return !(*this == rhs); }
    const bool operator== (const Range &rhs) const { 
      return ((this->min() == rhs.min()) && (this->max() == rhs.max()));  
    } 

    friend std::ostream& operator<< (std::ostream &os, const Range &range) { 
      os << "[INFO] Range: [" << range.min() << ", " << range.max() << "]" << std::endl;
      return os;
    }

  private:
    float m_min = 0.; // min and max values
    float m_max = 0.;
};


//===============================
// Class describing a DAQ run
//===============================
class Run
{
  public:
    Run() {};
    Run(const int Number, const RunType Type, const float HV) :
      number(Number),
      type(Type),
      hv(HV) {}

    // >< overload (for map sorting)
    const bool operator< (const Run &rhs) const { // sort by HV value
      if (this->hv < rhs.hv) return true; 
      else if (this->hv == rhs.hv) return this->number < rhs.number;
      else return false;
    } 
    const bool operator> (const Run &rhs) const { return !(*this < rhs); }

    friend std::ostream& operator<< (std::ostream &os, const Run &run) { 
      os << "[INFO] Run " << run.number << " info:" << std::endl;
      os << "[INFO] Type: " << run.type << std::endl;
      os << "[INFO] HV: " << run.hv << std::endl;
      return os;
    }

  public:
    int number = 0;          // run number
    RunType type = GenericRun; // run type
    float hv = 0.;           // HV (kV)
};


//==============================
// Data class
//==============================
// std::vector<float> with some utilities
class Data 
{
  public:
    Data() {};
    explicit Data(const int n) { m_data.resize(n, 0.0); };
    explicit Data(const int n, const float val) { m_data.resize(n, val); };
    Data(const float val) { m_data.push_back(val); };
    Data(const std::initializer_list<float> &values) { m_data = values; }
    Data(const Data &data) { this->add(data); };
    ~Data() {};

    // [] overloads (safe access)
    const float& operator[] (const int i) const {
      if (i < this->size()) return m_data[i];
      else { 
	std::cout << "[ERROR] Data::operator[]: " << i << " out of bounds!" << std::endl;
	abort();
      }
    }

    float& operator[] (const int i) {
      if (i < this->size()) return m_data[i];
      else { 
	std::cout << "[ERROR] Data::operator[]: " << i << " out of bounds!" << std::endl;
	abort();
      }
    }

    // * overload (multiply two data v, w elem. by elem.)
    // Two cases:
    // 1) Simple mult. by scalar: e.g. v = {1,2,3}, w = {2} => v*w = {2,4,6} 
    // 2) Mult. elem by elem.: e.g. v = {1,2,3}, w = {1,2,3,4,5} => v*w = {1,4,9}
    Data operator* (const Data &rhs) const {
      Data mult;
      const Data &longer = this->size() >= rhs.size() ? *this : rhs;
      const Data &shorter = rhs.size() <= this->size() ? rhs : *this;

      if (shorter.size() == 0) {
	std::cout << "[ERROR] Data::operator*: cannot multiply for an empty data!" << std::endl;
	abort();
      }
      else if (shorter.size() == 1) // simply mult. by scalar
	for (const auto &val : longer) mult.add( val * shorter[0] );
      else // mult. each elem. and returns a vector of the same size of the shorter one
	for (const auto &val : shorter) mult.add( val * longer[mult.size()] );

      return mult;
    }

    // / overload
    Data operator/ (const Data &rhs) const { 
      Data inverse;
      for (const auto &val : rhs) inverse.add( 1. / val );
      return *this * inverse;
    }

    // *= overload
    Data& operator*= (const Data &rhs) {
      *this = *this * rhs;
      return *this; 
    }

    // /= overload
    Data& operator/= (const Data &rhs) {
      *this = *this / rhs;
      return *this; 
    }

    // << overload (print class)
    friend std::ostream& operator<< (std::ostream &os, const Data &data) { 
      os << "[INFO] Data contains " << data.size() << " values:" << std::endl;
      for (int i = 0; i < data.size(); i++) 
        os << "[INFO] At " << i << ": " << data[i] << std::endl;   
      return os;
    }
    
    // Returns data 
    const std::vector<float> &data() const { return m_data; }
    std::vector<float> &data() { return m_data; }

    // Returns data size
    const int size() const { return m_data.size(); }

    // Returns data average
    const float average() const {
      if (this->size() == 0) {
	std::cout << "[WARNING] Data::average(): data is empty!" << std::endl;
	return 0;
      }
      float avg = std::accumulate(m_data.begin(), m_data.end(), 0.0) / this->size();
      return avg;
    }

    // Returns min/max 
    const float min() const { return this->range().min(); }
    const float max() const { return this->range().max(); }

    // Returns data sum
    const float sum() const { return this->average() * this->size(); }

    // Returns data range
    const Range range() const {
      if (this->size() == 0) {
	std::cout << "[WARNING] Data::range(): data is empty!" << std::endl;
	return {};
      }
      auto result = std::minmax_element(m_data.begin(), m_data.end()); 
      return {*result.first, *result.second};
    }

    // Return pointer to std::vector
    float* ptr() { return m_data.data(); }
    const float* ptr() const { return m_data.data(); }

    // Add vector of values
    void add(const std::vector<float> &values) 
    { m_data.insert(m_data.end(), values.begin(), values.end()); }

    // Add data
    void add(const Data &data) { this->add(data.data()); }

    // Clear data
    void clear() { m_data.clear(); }

    // Re-implement begin/end for range-based loop
    std::vector<float>::iterator begin() { return m_data.begin(); }
    std::vector<float>::iterator end() { return m_data.end(); }
    std::vector<float>::const_iterator begin() const { return m_data.begin(); }
    std::vector<float>::const_iterator end() const { return m_data.end(); }

  private:
    // Members
    std::vector<float> m_data = {}; 
};


//==========================================
// Template class for a generic std::map
//==========================================
// - Safe access:
//   Elements can be added only via Map::set(). 
//   Map::operator[] gives access only to exsisting keys (by reference) 
// - Add some utilities
template <class Key, class Data>
class Map
{
  public:
    Map() {};
    Map(const Key &key, const Data &data) { this->set(key, data); } 
    Map(const std::vector<TString> &keys) { for (const auto &key : keys) this->set(key); }
    Map(const Map &map) { for (const auto &elem : map) this->set(elem.first, elem.second); }
    ~Map() {};

    // [] overload (safe access)
    Data& operator[] (const Key &key) {
      if (this->contains(key)) return m_map[key];
      else { 
	std::cout << "[ERROR] Map::operator[]: key " << key << " not in map!" << std::endl;
	abort();
      }
    }

    const Data& operator[] (const Key &key) const {
      if (this->contains(key)) return m_map.at(key);
      else { 
	std::cout << "[ERROR] Map::operator[]: key " << key << " not in map!" << std::endl;
	abort();
      }
    }
    
    // << overload (print class)
    friend std::ostream& operator<< (std::ostream &os, const Map &map) { 
      os << "[INFO] Map contains " << map.nKeys() << " keys:" << std::endl;
      for (const auto key : map.keys()) {
        os << "[INFO] Key " << key << ": " << std::endl;
	os << map[key] << std::endl;   
      }
      return os;
    }

    // Returns keys
    const std::vector<Key>& keys() const { return m_keys; }

    // Returns num. defined keys 
    const int nKeys() const { return m_keys.size(); }

    // Tell if dataset contains a key
    const bool contains(const Key &key) const { return m_map.count(key); }

    // Returns sub-map w/ only the keys passed by argument
    const Map submap(const std::vector<Key> &keys) const {
      Map submap;
      for (const auto key : keys) {
	if (this->contains(key)) submap.set(key, (*this)[key]);
      }
      return submap;
    }

    // Set data to key
    void set(const Key &key, const Data &data = {}) { 
      if (!this->contains(key)) {
	m_keys.push_back(key); // add key if not existing
	std::sort(m_keys.begin(), m_keys.end(), // sort keys
	    [] (const Key& lhs, const Key& rhs){ return lhs < rhs; } );
      }
      m_map[key] = data; 
    }

    // Clear map
    void clear() { m_map.clear(); m_keys.clear(); }

    // Re-implement begin/end for range-based loop
    typename std::map<Key, Data>::iterator begin() { return m_map.begin(); }
    typename std::map<Key, Data>::iterator end() { return m_map.end(); }
    typename std::map<Key, Data>::const_iterator begin() const { return m_map.begin(); }
    typename std::map<Key, Data>::const_iterator end() const { return m_map.end(); }

  protected:
    // Members
    std::map<Key, Data> m_map = {};
    std::vector<Key> m_keys = {};
};


//==================
// DataMap class
//==================
// Map<TString, Data> with some utilities
class DataMap : public Map<TString, Data>
{
  public:
    DataMap() {};
    DataMap(const TString key, const Data &data) : Map(key, data) {}
    DataMap(const std::vector<TString> &keys) : Map(keys) {}
    DataMap(const Map<TString, Data> &map) : Map(map) {}
    DataMap(const DataMap &map) : Map(map) {}
    ~DataMap() {};

    // Returns data size
    const int size() const { 
      int size = 0.;
      for (auto const &elem : m_map) size += elem.second.size(); 
      return size;
    }

    // Returns data range
    const Range range() const {
      if (this->size() == 0) {
	std::cout << "[WARNING] DataMap::range(): map is empty!" << std::endl;
	return {};
      }

      float min = +99999999., max = -99999999.; // can be simplified using Range class
      for (auto const &elem : m_map) {
	Range range = elem.second.range();
	if (range.min() < min) min = range.min();
	if (range.max() > max) max = range.max();

      }
      return {min, max};
    }

    // Returns min/max 
    const float min() const { return this->range().min(); }
    const float max() const { return this->range().max(); }

    // Tell if it's made by one-value data
    const bool oneValued() const { return (this->size() == this->nKeys()); }
    
    // Add data to key
    void add(const TString key, const Data &data) { 
      if (!this->contains(key)) this->set(key, data); // if not exist, create it!
      else (*this)[key].add(data); // othrwise, increment data! 
    }
};


//==================
// MoniMap class
//==================
// Map<TString, DataMap> with some utilities
class MoniMap : public Map<TString, DataMap>
{
  public:
    using Map::set; // avoid name hiding
    using Map::contains;

    MoniMap() {};
    MoniMap(const TString key, const DataMap &dmap) : Map(key, dmap) {}
    MoniMap(const std::vector<TString> &keys) : Map(keys) {}
    MoniMap(const Map<TString, DataMap> &map) : Map(map) {} 
    MoniMap(const MoniMap &map) : Map(map) {}
    ~MoniMap() {};

    // Tell if it contains (key1, key2) 
    const bool contains(const TString key1, const TString key2) const {
      if (this->contains(key1)) {
	if ((*this)[key1].contains(key2)) return true;
      }
      return false; 
    }

    // Set data to (key1, key2)
    void set(const TString key1, const TString key2, const Data &data) { 
      if (!this->contains(key1)) this->set(key1, {key2, data}); // if key1 does not exists, create both
      else (*this)[key1].set(key2, data); // if key1 exists but key2 does not, create it
    }

    // Add data to (key1, key2)
    void add(const TString key1, const TString key2, const Data &data) { 
      if (!this->contains(key1, key2)) this->set(key1, key2, data); // if not exist, create it!
      else (*this)[key1][key2].add(data); // othrwise, increment data! 
    }
};
#endif

