#include "Plots.h"


///////////////////////
// Plot graph (Data) //
///////////////////////
TGraphErrors* plotGraph(const Data& x, 
                        const Data& y, 
                        const Data& ex, 
                        const Data& ey, 
                        const TString name,
                        const TString title,
                        const TString xtitle, 
                        const TString ytitle, 
                        Range yrange, 
                        const Color_t col, 
                        const TString opt) 
{
  if (x.size() != y.size()) {
    std::cout << "[WARNING] plotGraph(): " << name << " has different x, y sizes!" << std::endl; 
  }

  if (!yrange) yrange = y.range();
  if (yrange.isLowerInf()) yrange.setMin(y.range().min()); // prevent inf
  if (yrange.isUpperInf()) yrange.setMax(y.range().max());
  yrange.enlarge(RANGE_TOL);

  TGraphErrors *gr = new TGraphErrors(x.size(), x.ptr(), y.ptr(), ex.ptr(), ey.ptr());
  gr->SetName(("Graph " + name).ReplaceAll(" ", "_"));
  gr->GetYaxis()->SetTitleOffset(1.3);
  gr->SetTitle(title + ";" + xtitle + ";" + ytitle);
  gr->SetMarkerStyle(21);
  gr->SetMarkerSize(2);
  gr->SetMarkerColor(col);
  gr->SetLineColor(col);
  gr->SetLineWidth(2.0);
  gr->SetMinimum( yrange.min() );
  gr->SetMaximum( yrange.max() );
  gr->Draw(opt);
  gr->Write(); // ROOT does not save TGraphs automatically...

  return gr;
}


//////////////////////////
// Plot graph (DataMap) //
//////////////////////////
TGraphErrors* plotGraph(const DataMap &xmap, 
                        const DataMap &ymap,
                        const DataMap &exmap,
                        const DataMap &eymap,
                        const TString name,
                        const TString title, 
                        const TString xtitle, 
                        const TString ytitle, 
                        const Range range, 
                        const Color_t col, 
                        const TString opt)
{
  Data x, y, ex, ey; 
  for (const auto &data : xmap) x.add(data.second);
  for (const auto &data : ymap) y.add(data.second);
  for (const auto &data : exmap) ex.add(data.second);
  for (const auto &data : eymap) ey.add(data.second);

  TGraphErrors *gr = plotGraph(x, y, ex, ey, name, title, xtitle, ytitle, range, col, opt);
  return gr;
}


//////////////////////////////
// Plot 1D histogram (Data) //
//////////////////////////////
TH1F* plot1DHisto(const Data &data, 
                  const TString name, 
                  const TString title, 
                  const TString xtitle, 
		  Int_t nBins,
		  Range range,
                  const Color_t col,
                  const TString opt) 
{
  // Binning (if not range provided/wrong, use data range)
  if (!range) range = data.range();
  if (range.isLowerInf()) range.setMin(data.range().min()); // prevent inf
  if (range.isUpperInf()) range.setMax(data.range().max());
  range.enlarge(RANGE_TOL);
  Float_t xmin = range.min(); 
  Float_t xmax = range.max(); 
  if (nBins == 0) nBins = data.size(); 

  // Book histogram
  TH1F *h = new TH1F(("Histo1D " + name).ReplaceAll(" ", "_"), title, nBins, xmin, xmax);
  h->SetLineWidth(2);
  h->SetLineColor(col);
  h->SetMarkerColor(col);
  h->GetYaxis()->SetTitleOffset(1.3);
  h->SetTitle(title + ";" + xtitle);

  // Fill histogram
  for (const auto &val : data) h->Fill(val); 

  // Draw it
  h->Draw(opt);

  return h;
}


/////////////////////////////////
// Plot 1D histogram (DataMap) //
/////////////////////////////////
TH1F* plot1DHisto(const DataMap &dmap, 
                  const TString name, 
                  const TString title, 
                  const TString xtitle, 
		  const Int_t nBins,
		  const Range range,
                  const Color_t col,
                  const TString opt) 
{
  Data totdata;
  for (const auto &data : dmap) totdata.add(data.second);

  TH1F* h = plot1DHisto(totdata, name, title, xtitle, nBins, range, col, opt);
  return h;
}


//////////////////////////////
// Plot 2D histogram (Data) //
//////////////////////////////
TH2F* plot2DHisto(const Data &xdata, 
                  const Data &ydata, 
                  const TString name, 
                  const TString title, 
                  const TString xtitle, 
                  const TString ytitle, 
                  const TString opt)

{
  gStyle->SetOptStat(0);

  // Binning
  Range xrange = xdata.range();
  xrange.enlarge(RANGE_TOL);
  Float_t xmin = xrange.min(); 
  Float_t xmax = xrange.max(); 
  Int_t nBinx = xdata.size(); 

  Range yrange = ydata.range();
  yrange.enlarge(RANGE_TOL);
  Float_t ymin = yrange.min(); 
  Float_t ymax = yrange.max(); 
  Int_t nBiny = ydata.size(); 

  // Book histogram
  TH2F *h = new TH2F(("Histo2D " + name).ReplaceAll(" ", "_"), title, nBinx, xmin, xmax, nBiny, ymin, ymax);
  h->GetYaxis()->SetTitleOffset(1.3);
  h->SetTitle(title + ";" + xtitle + ";" + ytitle);

  // Fill histogram
  for (const auto &xval : xdata) {
    for (const auto &yval : ydata) {
      h->Fill(xval, yval);
    }
  }

  // Draw it
  h->Draw(opt);

  return h;
}
