#ifndef MONITOR_H
#define MONITOR_H 1

#include "Data.h" 
#include "Plots.h"

// C++ inc
#include <iomanip>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iterator>

// ROOT inc
#include "TROOT.h"
#include "TSystem.h"
#include "TStyle.h"
#include "TTree.h"
#include "TPad.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TF1.h"
#include "TMath.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TBox.h"
#include "TLine.h"
#include "TGaxis.h"
#include "TPaletteAxis.h"
#include "TText.h"

// Useful constants
#define PMTDIM 8
#define BOXDIM 4
#define NPMT 16
#define NBOARDS 8
#define NPX PMTDIM*PMTDIM


//==============================
// Base class for Q&A monitor 
//==============================
class Monitor 
{
  public:
    // Cons/destr
    Monitor(const TString name);
    ~Monitor();

    // Read monnitor config 
    void readConf(const TString configfile);

    // Read mapping from text file
    void readMapping(const TString filename);

    // Read datseheet (.csv format) 
    void readDatasheet(const TString filename);

    // Set monitor name 
    void setName(TString name) { m_name = name.ReplaceAll(" ", "_"); }

    // Set monitor config name
    void setConfigName(TString name) { m_configName = name.ReplaceAll(" ", "_"); }

    // Set working directory
    void setWorkDir(const TString path) { m_workDir = path; }

    // Set board (pos, address) 
    // Position in front of the box:
    // |A|B| |C|D|
    // |E|F| |G|H|
    void setFeb(const TString pos, const TString addr); 
    
    // Set PMT (name, pos, MAROC gain) 
    void setPMT(const TString pmt, const PmtPos pos, const Int_t gain); 

    // Set tolerance range for variable
    void setTolerance(const TString, const Float_t min, const Float_t max);

    // Set if to run the analysis
    void setRunAnalysis(const bool flag);
    void setRunAnalysis(const TString flag);

    // Add runs to monitor
    void addRun(const Int_t runNb, const TString comment = "");

    // Returns runs
    const std::vector<Run>& runs() const { return m_data.keys(); }

    // Returns runs of given type
    std::vector<Run> runs(RunType type) const;

    // Returns PMTs 
    const std::vector<TString>& pmts() const { return m_pmtMap.keys(); }

    // Delete runs
    void deleteRuns();

    // Print list of monitored runs
    void printRuns() const;

    // Print datsheet values for MaPMTs
    void printDatasheet() const;

    // Run monitor
    void run();

    // Analyze monitored data (user-defined)
    virtual void analyze() = 0;


  protected: 
    // Plot all plots needed (user-defined)
    virtual void makePlots(const Run run) = 0;

    // Process input data (user-defined)
    virtual void processData(const Run run, const TString pmt) = 0;

    // Monitor a given run
    void monitor(const Run run);

    // Fill monitoring variable and related quantities (mean, ...)
    void fillMoniVar(const Run run, const TString var, const TString pmt, const Data &data);

    // Tell if PMT is available in datasheet
    bool isInDatasheet(const TString pmt) { return m_datasheet.begin()->second.contains(pmt); }

    // Make monitoring plots of the run
    void plot(const Run run);

    // Write monitoring info on file
    void write(const Run run);

    // Plot variable on graph for all PMTs for a run
    TGraphErrors* plotMoniGraph(const Run run, const TString var, 
                                const TString yunit, 
	                        const Color_t col = kBlue, const TString opt = "AP");

    // Plot variable on graph for a PMT for all runs
    TGraphErrors* plotMoniGraph(const TString var, const TString pmt, 
                                const TString xunit, const TString yunit, 
	                        const Color_t col = kBlue, const TString opt = "AP", 
                                const Float_t xstart = 0., const Float_t step = 1.);

    // Plot variable on 1D histogram
    void plotMoniHisto(const Run run,
	               const TString var, 
	               const TString xtitle, 
		       Int_t nBins = 0,
		       Range range = {},
	               const Color_t col = kBlue, 
	               const TString opt = "");

    // Plot the map of variable on the test-box 
    TH2F* plotMoniMap(const Run run, 
	              const TString var, 
	              const TString zunit = "",
		      const TString opt = "COLZ",
	              const bool draw_pmt_name = false);
	              //const bool new_canv = true); 

    // Plot correlation graph wrt datasheet
    void plotMoniCorrel(const Run run,
                        const TString dsvar, const TString var,
                        const TString xunit, const TString yunit);

    // Plot analysis variable on 1D histogram
    void plotAnaHisto(const TString val, 
                      const TString xtitle, 
		      Int_t nBins = 0,
		      Range range = {},
       	              const Color_t col = kBlue, 
		      const TString opt = "");

    // Plot test-box on canvas
    void plotBox(const TString opt = "",
	         const bool draw_pmt_name = false); 

    // Plot variable tolerance on histogram
    void plotTolerance(const TString var, TH1F* h);
    
    // Plot variable tolerance on graph
    void plotTolerance(const TString var, TGraphErrors* gr);

    // Returns PMT position on the test-box
    std::pair<Int_t, Int_t> posOnBox(const TString pmt);

    // Returns charge (Me-) for a given MAROC gain and ADC value
    Float_t charge(Float_t G_MAROC, Float_t G_ADC) 
    {
      Float_t c = -8.758;
      Float_t d = 1.302;
      Float_t e = -0.0014;
      Float_t f = -0.565;
      Float_t g = 0.013;
      Float_t h = 0.0002;

      if (G_MAROC == 0. || G_ADC == 0.) return 0.;

      Float_t charge = G_ADC / (c + d*G_MAROC + e*pow(G_MAROC,2))
	- ((f + g*G_MAROC + h*pow(G_MAROC,2)) / (c + d*G_MAROC + e*pow(G_MAROC,2))); 

      // Charge in Me-
      charge *= 10. / 1.6; // 1e-12 (pF) / 1.6e-19 (Qe-)
      return charge;
    }
     
    Data charge(Float_t G_MAROC, const Data& adcs) 
    {
      Data qs; 
      for (const auto adc : adcs) qs.add(charge(G_MAROC, adc)); 
      return qs;
    }
    
    // Members
    TString m_name;                       // Monitor name
    TString m_configName;                 // Name of config. to monitor
    TString m_workDir;                    // Working directory
    TString m_runDir;                     // Directory of the current run
    TFile *m_outFile;                     // Output file
    TFile *m_anaFile;                     // Output analysis file

    bool m_runAnalysis;                   // Flag to run the analysis 

    Map<TString, TString> m_febMap;       // Map feb addr -> box pos 
    Map<TString, PmtPos> m_pmtMap;        // Map pmt -> feb pos 
    Map<TString, Range> m_tol;            // Map var -> range
    Map<TString, Int_t> m_MAROCGain;      // Map pmt -> MAROC gain

    Map<Run, MoniMap> m_data;             // Map run -> data
    MoniMap m_anaData;                    // Analyized data
    MoniMap m_datasheet;                  // Datasheet data
};
#endif
