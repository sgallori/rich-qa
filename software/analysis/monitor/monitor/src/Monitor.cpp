#include "Monitor.h"


/////////////////
// Constructor //
/////////////////
Monitor::Monitor(const TString name) 
{
  std::cout << "==================================================" << std::endl;
  std::cout <<  name  << " Monitor                                " << std::endl;
  std::cout << "==================================================" << std::endl;
  
  setName(name);
  gROOT->SetBatch(); 
  setRunAnalysis(false);
}


////////////////
// Destructor //
////////////////
Monitor::~Monitor() {};


/////////////////////////
// Read monitor config // 
/////////////////////////
void Monitor::readConf(const TString configfile) 
{
  std::ifstream file(configfile.Data(), std::ios::in);
  std::string line;
  
  if (!file.good()) {
    std::cout << "[ERROR] readConf(): Cannot find config file: " << configfile << std::endl;
    abort();
  }

  // Read config file
  std::cout << "[INFO] Reading config file: " << configfile << std::endl;

  while (std::getline( file, line )) {
    if (line.empty()) continue; // Skip empty line
    if (TString(line).BeginsWith("#")) continue; // Skip comment

    std::istringstream iss( line );
    TString tag, value;
    std::string extra;
    iss >> tag >> value;
    std::getline(iss, extra);
    
    if (tag == "ConfName") setConfigName(value);
    else if (tag == "Workdir") setWorkDir(value);
    else if (tag == "Mapping") readMapping(value);
    else if (tag == "Datasheet") readDatasheet(value);
    else if (tag == "Run") addRun(value.Atoi(), extra);
    else if (tag == "RunAnalysis") setRunAnalysis(value);
    else {
      std::cout << "[WARNING] readConf(): " << tag << " not a config parameter!" << std::endl;
      continue;
    }
  }
}


//////////////////
// Read mapping //
//////////////////
void Monitor::readMapping(const TString filename) 
{
  std::ifstream file(filename.Data(), std::ios::in);
  std::string line;
  
  if (!file.good()) {
    std::cout << "[ERROR] readMapping(): Cannot find map: " << filename << std::endl;
    abort();
  }

  // Read file containing map
  std::cout << "[INFO] Reading mapping: " << filename << std::endl;

  while (std::getline( file, line )) {
    if (line.empty()) continue; // Skip empty line
    if (TString(line).BeginsWith("#")) continue; // Skip comment

    std::istringstream iss( line );
    TString febpos, febaddr, pmtNb, pmt, gain;
    iss >> febpos >> febaddr >> pmtNb >> pmt >> gain;

    // Skip board w/o pmt
    if (pmt == "") continue; 

    //// Check if MAROC gain is read
    //if (pmt != "" && gain == "") {
    //  std::cout << "[ERROR] readMapping(): No MAROC gain read for " << pmt << "!" << std::endl;
    //  abort();
    //}

    // Set feb
    setFeb(febpos, febaddr);

    // Set PMT
    PmtPos pmtpos = {febaddr, pmtNb.Atoi()};
    setPMT(pmt, pmtpos, gain.Atoi());
  }   

  file.close();
}


////////////////////
// Read datasheet // 
////////////////////
void Monitor::readDatasheet(const TString datasheet)
{
  const size_t nDataPerPmt = 7 + NPX; // data per PMT
  std::string line;
  std::vector<std::string> bad_headers = {"parameter", "unit", "error(max)", "failure"};

  std::ifstream file( datasheet, std::ios::in );
  if (!file.good()) {
    std::cout << "[ERROR] readDatasheet(): Cannot find datasheet: " << datasheet << std::endl;
    abort();
  }

  // Read datasheet
  std::cout << "[INFO] Reading datasheet: " << datasheet << std::endl;

  while (std::getline(file, line)) {
    if (line.empty()) continue; // Skip empty line

    // Tokenize line
    std::istringstream iss( line );
    std::string token;
    std::vector<std::string> tokens; 
    while (std::getline(iss, token, ',')) {
      if (token != "") {
	token.erase(std::remove_if(token.begin(), token.end(), isspace), token.end());
	tokens.push_back(token);
      }
    }
    size_t nTokens = tokens.size();
    if (nTokens == 0) continue; // Skip line w/o tokens

    // Skip lines we don't want
    std::string header = tokens[0];
    if (std::find(bad_headers.begin(), bad_headers.end(), header) != bad_headers.end()) continue;

    // Load datasheet specs 
    if (nTokens == nDataPerPmt) {
      std::string pmt = header;
      m_datasheet.add("Sk", pmt, std::stod(tokens[1]));
      m_datasheet.add("Sp", pmt, std::stod(tokens[2]));
      m_datasheet.add("Dark current", pmt, std::stod(tokens[3]));
      m_datasheet.add("Skb", pmt, std::stod(tokens[4]));
      m_datasheet.add("Average Gain", pmt, std::stod(tokens[5]));
      m_datasheet.add("Uniformity", pmt, std::stod(tokens[6]));
      for (size_t ipx = 0; ipx < NPX; ipx++) {
	Float_t gain = std::stod(tokens[7 + ipx]);
	m_datasheet.add("Normalized Gain", pmt, gain);
      }
      Float_t dark_rate = std::stod(tokens[3]) / 1.6e-4 / std::stod(tokens[5]);
      m_datasheet.add("Dark current rate", pmt, dark_rate / 1e3); // kHz
    }

    else {
      std::cout << "[ERROR] readDatasheet(): Cannot parse datasheet! Did you change format?" << std::endl;
      std::cout << "[ERROR] readDatasheet(): Cannot parse this line: " << line << std::endl; 
      std::cout << "[ERROR] readDatasheet(): Expected tokens = " << nDataPerPmt << ", found = " << nTokens << std::endl; 
      abort(); 
    }
  }//file
}


/////////////////////////////////////
// Set PMT (name, pos, MAROC gain) //
/////////////////////////////////////
void Monitor::setPMT(const TString pmt, const PmtPos pos, const Int_t gain) 
{
  // Check pmt number on board
  if (pos.second != 1 && pos.second != 2) {
    std::cout << "[ERROR] setPMT(): PMT position on feb should be 1 or 2!" << std::endl; 
    abort();
  }

  // Check MAROC gain
  if (gain < 0 || gain > 256) {
    std::cout << "[ERROR] setPMT(): MAROC gain outside range [0-256]!" << std::endl; 
    abort();
  }

  //m_pmts.push_back(pmt);
  m_pmtMap.set(pmt, pos);
  m_MAROCGain.set(pmt, gain);

  std::cout << "[INFO] PMT " << pmt << " set on feb " << pos.first
    << " pos " << pos.second << " (MAROC gain = " << gain << ")" << std::endl; 
}


//////////////////////////////
// Set board (pos, address) //
//////////////////////////////
void Monitor::setFeb(const TString pos, const TString addr) 
{ // Board naming convention:
  // |A|B| |C|D|
  // |E|F| |G|H|
  if (pos != "A" && pos != "B" && pos != "C" && pos != "D" && 
      pos != "E" && pos != "F" && pos != "G" && pos != "H") {
    std::cout << "[ERROR] setBoardAddress(): Board position " << pos << " not allowed." << std::endl;
    abort();
  }

  m_febMap.set(addr, pos);
  std::cout << "[INFO] Board " << addr << " set in pos. " << pos << std::endl;
}


//////////////////////////////////////
// Set tolerance range for variable //
//////////////////////////////////////
void Monitor::setTolerance(const TString var, const Float_t min, const Float_t max) 
{
  if (min >= max) {
    std::cout << "[ERROR] setTolerance(): Triyng to set" << var << " with wrong range!" << std::endl;
    abort();
  }
  m_tol.set(var, {min, max});
  std::cout << "[INFO] " << var << " set with tolerance [" << min << ", " << max << "]" << std::endl;
}


///////////////////////////////////
// Decide if to run the analysis //
///////////////////////////////////
void Monitor::setRunAnalysis(const bool flag) 
{
  m_runAnalysis = flag;

  std::cout << "[INFO] RunAnalysis flag set to " 
    << (m_runAnalysis == true ? "true" : "false") << std::endl;
}


void Monitor::setRunAnalysis(const TString flag)
{
  if (flag == "y" || flag == "yes" || flag == "Yes") 
    setRunAnalysis(true);
  else 
    setRunAnalysis(false);
}


/////////////////////////
// Add runs to monitor //
/////////////////////////
// It could be a better idea to force comment to be exactly one of the following strings:
// Dark counts, Calib run, 1kV, ..., Bad 
void Monitor::addRun(const Int_t runNb, const TString comment) 
{ 
  RunType type;
  Float_t hv = 0.;
  TString::ECaseCompare ignore = TString::ECaseCompare::kIgnoreCase;

  // Parse comment
  if (comment.Contains("Dark", ignore)) type = DarkRun; 
  else if (comment.Contains("Calib", ignore)) type = CalibRun; 
  else if (comment.Contains("LED", ignore)) {
    type = LEDRun; 
    if (comment.Contains("1.10")) hv = 1.1;
    if (comment.Contains("1.05")) hv = 1.05;
    if (comment.Contains("1.00")) hv = 1.0;
    if (comment.Contains("0.95")) hv = 0.95;
    if (comment.Contains("0.90")) hv = 0.9;
    if (comment.Contains("0.85")) hv = 0.85;
  }
  else if (comment.Contains("Bad", ignore)) type = BadRun;
  else type = GenericRun;

  // Add run 
  if (type != BadRun) {
    Run run(runNb, type, hv); 
    m_data.set(run);
  }
  else {
    std::cout << "[INFO] Run " << runNb << " is bad. Ignored" << std::endl;
  }

  std::cout << "[INFO] Run " << runNb << " " << comment
    << " added (type: " << type << ")" << std::endl;
}


//////////////////////////////////
// Print list of monitored runs //
//////////////////////////////////
void Monitor::printRuns() const
{ 
  std::cout << "[INFO] Runs to be monitored:" << std::endl;
  for (const auto &run : runs()) 
    std::cout << "[INFO] " << run.number << std::endl; 
}


//////////////////
 // Delete runs //
//////////////////
void Monitor::deleteRuns() 
{ 
  m_data.clear(); 
  //m_runs.clear(); 
  setRunAnalysis(false);
  std::cout << "[INFO] All runs have been deleted" << std::endl;
}


////////////////////////////////
// Returns runs of given type //
////////////////////////////////
std::vector<Run> Monitor::runs(RunType type) const
{ 
  std::vector<Run> type_runs;
  for (const auto run : runs()) 
    if (run.type == type) type_runs.push_back(run);
  return type_runs;
}


//////////////////////////////
// Fill monitoring variable //
//////////////////////////////
void Monitor::fillMoniVar(const Run run, const TString var, const TString pmt, const Data &data)
{
  // Add the data
  MoniMap &mmap = m_data[run]; // get current DataMap  
  mmap.add(var, pmt, data);

  // Add average, min/max values
  mmap.add("Average " + var, pmt, data.average());
  mmap.add("Total " + var, pmt, data.sum());
  mmap.add("Min " + var, pmt, data.min());
  mmap.add("Max " + var, pmt, data.max());
}


////////////////////////////////////////
// Write monitoring info on text file //
////////////////////////////////////////
void Monitor::write(const Run run) 
{
  TString fname = Form("%s/%s_monitor_data.dat", m_runDir.Data(), m_name.Data());
  FILE *outfile = fopen(fname.Data(), "w");

  // Loop on datsets
  for (const auto &mmap : m_data[run]) {
    fprintf(outfile, "%s \n", mmap.first.Data()); // write var 

    // Loop on data
    for (const auto &dmap : mmap.second) { 
      fprintf(outfile, "%s \n", dmap.first.Data()); // write pmt

      // Loop on values
      size_t nVal = dmap.second.size();
      for (size_t i = 0; i < nVal; i++) {
        fprintf(outfile, "%zu %f \n", i, dmap.second[i]); // write values 
      }//val
    }//data
  }//dset

  fclose (outfile);
}


///////////////////////////
// Make monitoring plots //
///////////////////////////
void Monitor::plot(const Run run)
{
  m_outFile = new TFile(Form("%s/%s_monitor_plots.root", 
	m_runDir.Data(), m_name.Data()), "RECREATE");

  makePlots(run);

  m_outFile->Write(); 
  m_outFile->Close();
}


////////////////////////////
// Print datasheet values //
////////////////////////////
void Monitor::printDatasheet() const
{
  for (const auto &pmt : pmts()) {
    std::cout << "[INFO] PMT " << pmt << " has datasheet values:" << std::endl;

    for (const auto &var : m_datasheet.keys()) {
      std::cout << "[INFO] " << var << " ";
      for (const auto &val : m_datasheet[var][pmt]) std::cout << val << " ";
      std::cout << std::endl; 
    }//var

    std::cout << std::endl; 
  }//pmt
}


/////////////////
// Monitor run //
/////////////////
void Monitor::monitor(const Run run)
{ 
  // Start!
  time_t start, end;
  time( &start );

  std::cout << "==================================================" << std::endl;
  std::cout << "[INFO] Monitoring run: " << run.number              << std::endl;
  std::cout << "==================================================" << std::endl;

  // Set current directory
  //m_runDir = Form("%s/run0%i", m_workDir.Data(), run.number); 
  m_runDir = Form("%s/%i", m_workDir.Data(), run.number); 

  // Process data
  for (const auto &pmt : pmts()) {
    std::cout << "[INFO] Processing PMT: " << pmt << std::endl;
    if (!isInDatasheet(pmt)) {
      std::cout << "[WARNING] monitor(): PMT " << pmt << " is not in datasheet!" << std::endl;
    }
    processData(run, pmt);
  }//pmt

  // Make monitoring plots 
  plot(run);

  // Save info on txt
  write(run);
  
  // Stop!
  time( &end );
  int timing = difftime( end, start );

  std::cout << "[INFO] Took me: " << int(timing / 60) << " min" 
            << " and " << timing % 60 << " sec" << std::endl;
  std::cout << "[INFO] Done!" << std::endl;
  std::cout << std::endl;
}


/////////////////////
// Run the monitor //
/////////////////////
void Monitor::run() 
{
  // Start!
  time_t start, end;
  time( &start );

  std::cout << "==================================================" << std::endl;
  std::cout << "[INFO] Monitor started!"                            << std::endl;
  std::cout << "==================================================" << std::endl;

  printRuns();

  // Monitor each run
  for (const auto &run : runs()) monitor(run);

  // Run analysis
  if (m_runAnalysis == true) {
    m_anaFile = new TFile(Form("%s/%s_monitor_analysis_plots_%s.root", 
	  m_workDir.Data(), m_name.Data(), m_configName.Data()), "RECREATE");

    std::cout << "==================================================" << std::endl;
    std::cout << "[INFO] Analysis started!"                           << std::endl;
    std::cout << "==================================================" << std::endl;
    analyze();
    
    m_anaFile->Write();
    m_anaFile->Close();

    // Write analysis info
    TString fname = Form("%s/%s_monitor_analysis_data_%s.dat", 
	m_workDir.Data(), m_name.Data(), m_configName.Data());
    FILE *outfile = fopen(fname.Data(), "w");

    // Loop on datsets
    for (const auto var : m_anaData.keys()) {
      fprintf(outfile, "%s \n", var.Data()); // write var 

      // Loop on data
      for (const auto pmt : m_anaData[var].keys()) { 
	fprintf(outfile, "%s \n", pmt.Data()); // write pmt

	// Loop on values
	unsigned ival(0);
	for (const auto val : m_anaData[var][pmt]) {
	  fprintf(outfile, "%i %f \n", ++ival, val); // write values 
	}//val
      }//pmt
    }//var

    fclose (outfile);
  }

  // Stop!
  time( &end );
  int timing = difftime( end, start );

  // Print timing
  std::cout << "[INFO] Total monitoring time: " << int(timing / 60) << " min" 
            << " and " << timing % 60 << " sec" << std::endl;
  std::cout << "[INFO] Done!" << std::endl;
}



