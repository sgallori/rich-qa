#ifndef ONLINE_MONITOR_H
#define ONLINE_MONITOR_H 1

#include "Monitor.h"
#include "TSpectrum.h"
#include "TPolyMarker.h"
#include <sys/stat.h>


//=================
// Online monitor 
//=================
class OnlineMonitor: public Monitor
{		    
  public:
    OnlineMonitor(TString name = "Online Monitor") : Monitor(name) {}
    ~OnlineMonitor() {};

    // Process input data (user-defined)
    void processData(const Run run, const TString pmt);

    // Analyze data (user-defined)
    void analyze();

    // Make monitoring plots (user-defined)
    void makePlots(const Run run);

  private:
    Map<TString, std::vector<TH1F*>> m_hspectra; // Map with spectrum histograms
};
#endif
