#include "OnlineMonitor.h"

//========================
// Run Online Monitor 
//========================
int main(int argc, char **argv)
{
  Int_t run = 0;
  TString runinfo = "";
  TString configfile(argv[1]);
  if (argc >= 3) { 
    run = atoi(argv[2]); 
    for (int i = 3; i < argc; i++) 
      runinfo += TString(argv[i]) + " "; 
  }

  // The monitor
  Monitor *moni = new OnlineMonitor("Online");

  // Read config
  moni->readConf(configfile);
  //moni->printDatasheet();

  // Define tolerances
  moni->setTolerance("Average Gain", 1., HUGE_VAL); // Me-
  moni->setTolerance("Uniformity", 25., 100.); 
  moni->setTolerance("Total Dark current", 0., 4.); // nA
  moni->setTolerance("Total Dark count rate", 0., 16.); // kHz
  moni->setTolerance("Peak to valley ratio", 1.3, HUGE_VAL); 

  moni->setTolerance("Dark count rate", 0., 1.); // kHz
  moni->setTolerance("Signal fraction", 0., 1e-3);
  moni->setTolerance("Pedestal pos", 15., 18.);
  moni->setTolerance("Gain ADC", 10., 120.);
  
  // Run monitor!
  if (run) {
    moni->deleteRuns(); // remove read runs
    moni->addRun(run, runinfo);
  }
  moni->run();

  return 0;
}
