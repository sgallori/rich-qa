#include "OnlineMonitor.h"


////////////////
// Make plots //
////////////////
void OnlineMonitor::makePlots(const Run run)
{
  // Plot ADC spectra
  const int npadx = 3; const int npady = 3; const int npad = npadx*npady; 
  for (const auto &pmt : pmts()) {
    TCanvas *canv = new TCanvas("canv_" + pmt, "Spectra", 800, 600);
    canv->Divide(npadx, npady);
    TString pdfname = m_runDir + "/" + pmt + ".pdf";
    canv->SaveAs(pdfname + "[");

    for (int ipx = 0; ipx < NPX; ipx++) {
      int ipad = (ipx%npad) + 1;
      TCanvas *ctmp = (TCanvas*)canv->cd(ipad);
      ctmp->SetLogy();

      m_hspectra[pmt][ipx]->Draw();
      m_hspectra[pmt][ipx]->Write();

      if (ipad == npad) {
	canv->SaveAs(pdfname);
	canv->Clear();
	canv->Divide(npadx, npady);
      }
    }//ipx

    if (NPX%npad != 0) canv->SaveAs(pdfname); // save remaining spectra
    canv->SaveAs(pdfname + "]");
  }//pmt

  for (auto &pmt : pmts()) // delete histos
    for (int ipx = 0; ipx < NPX; ipx++) delete m_hspectra[pmt][ipx];


  // Plot other variables
  TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1300, 800);
  TString pdfname = Form("%s/%s_monitor_summary.pdf", m_runDir.Data(), m_name.Data());
  canv->SaveAs(pdfname + "[");

  if (run.type == CalibRun) {
    canv->Divide(3, 2);
    canv->cd(1);
    plotBox("", true);

    canv->cd(2);
    plotMoniMap(run, "Gain ADC", "[ADC]");
    plotMoniMap(run, "Average Gain ADC", "[ADC]", "text same");

    canv->cd(3);
    plotMoniMap(run, "Average MAROC gains", "[ADC]", "text");
    canv->SaveAs(pdfname);
    canv->Clear();
  }

  if (run.type == LEDRun) {
    canv->Divide(3, 2);
    canv->cd(1);
    plotBox("", true);
    
    canv->cd(2);
    plotMoniMap(run, "Pedestal pos");

    canv->cd(3);
    plotMoniMap(run, "Gain ADC", "[ADC]");
    plotMoniMap(run, "Average Gain ADC", "[ADC]", "text same");

    canv->cd(4);
    plotMoniMap(run, "Gain");
    plotMoniMap(run, "Average Gain", "", "text same");

    canv->cd(5);
    plotMoniCorrel(run, "Average Gain", "Average Gain", "[x10^{6}]", "[x10^{6}]");

    canv->cd(6);
    plotMoniCorrel(run, "Average Gain", "Uniformity", "[x10^{6}]", "");
    canv->SaveAs(pdfname);
    canv->Clear();

    plotMoniHisto(run, "Peak to valley ratio", "Peak to valley ratio");
    canv->SaveAs(pdfname);
    canv->Clear();
  }

  if (run.type == DarkRun) { 
    canv->Divide(3, 2);
    canv->cd(1);
    plotBox("", true);

    canv->cd(2);
    plotMoniMap(run, "Dark count rate", "[kHz]");
    plotMoniMap(run, "Total Dark count rate", "", "text same");
    
    canv->cd(3);
    plotMoniCorrel(run, "Dark current", "Total Dark current", "[nA]", "[nA]");
    
    canv->cd(4);
    plotMoniCorrel(run, "Dark current", "Total Dark count rate", "[nA]", "[kHz]");
    
    canv->cd(5);
    plotMoniCorrel(run, "Average Gain", "Total Dark current", "[x10^{6}]", "[nA]");
    
    canv->cd(6);
    plotMoniCorrel(run, "Average Gain", "Total Dark count rate", "[x10^{6}]", "[kHz]");
    canv->SaveAs(pdfname);
    canv->Clear();
  }
    
  canv->SaveAs(pdfname + "]");
  canv->Write();
}


/////////////////////////////
// Process data of a MaPMT //
/////////////////////////////
void OnlineMonitor::processData(const Run run, const TString pmt)
{
  const Float_t xmin = 0., xmax = 256.; // spectrum histo. binning
  const Int_t nBins = xmax - xmin;
  const Float_t dist_ped = 5.;          // ADC distance to pedestal for dark counts
  const Int_t MAROCGain0 = 64;          // initial MAROC gain
  const Int_t ADC_max = 256;            // max ADC value we want
  const Float_t shaping_time = 500e-9;  // MAROC shaping time (sec) 
  const Float_t thr = 1e-3;             // find peaks w/ height >= thr*max_height 	
  const Float_t sigma = 3.;             // separation between peaks 

  Data gains_adc, peakvalley_ratios;
  Data pedestal_pos, signal_frac;
  Data dark_rates, dark_rate_errors;
  Data MAROCGains;
  UShort_t PMT_ADC[NPX]; 

  TSpectrum *s = new TSpectrum(10); 

  // Open data file 
  TString file = Form("%s/%s_000%i.root", m_runDir.Data(), pmt.Data(), run.number); // assume 000!!!
  if (gSystem->AccessPathName(file)) {
    std::cout << "[ERROR] processData(): File " << file << " does not exist!" << std::endl;
    abort();
  }
  TFile *rootfile = TFile::Open(file, "read");   
  TTree *tree = (TTree*)rootfile->Get("mdfTree");
  Int_t nEvents = tree->GetEntries(); 

  // Load ADC values
  TString branchName = "B01_PMT1_ADC"; 
  tree->SetBranchStatus("*", 0);
  tree->SetBranchStatus(branchName, 1);
  tree->SetBranchAddress(branchName, &PMT_ADC[0]);

  // Book spectrum histo
  m_hspectra.set(pmt);
  std::vector<TH1F*> &hspectra = m_hspectra[pmt];
  for (int ipx = 0; ipx < NPX; ipx++) {
    TString hname = Form("hdata_%s_px%i", pmt.Data(), ipx);
    TH1F *h = new TH1F(hname, Form("Spectrum PMT %s px%i", pmt.Data(), ipx), nBins, xmin, xmax);
    hspectra.push_back(h);
  }

  // Fill spectrum histos (much faster!)
  for (int iev = 0; iev < nEvents; iev++) {
    tree->GetEvent(iev); 
    for (int ipx = 0; ipx < NPX; ipx++) {
      hspectra[ipx]->Fill( Float_t(PMT_ADC[ipx]) ); 
    }
  }//evts

  // Now process each pixel
  for (int ipx = 0; ipx < NPX; ipx++) {
    TH1F *hdata = hspectra[ipx];

    // Run peaks finding
    Int_t npeaks = s->Search(hdata, sigma, "nodraw", thr); 
    Double_t *x_peaks = s->GetPositionX();
    Double_t *y_peaks = s->GetPositionY();
    Double_t ped = 0., gain_adc = 0., ypeak = 0., yvalley = 0., xvalley = 0.;
    if (npeaks <= 0) 
      std::cout << "[WARNING] processData(): PMT " << pmt << ", pixel " << ipx + 1 << " w/o peaks!" << std::endl;
    else {
      ped = x_peaks[0];

      if (npeaks > 1) {
	if (npeaks > 2 && x_peaks[1] <= ped) {
	  std::cout << "[WARNING] processData(): PMT " << pmt << ", pixel " << ipx + 1 
	    << " has second peak below pedestal. Using third peak!" << std::endl; s->Print("v");
	  x_peaks[1] = x_peaks[2];
	  y_peaks[1] = y_peaks[2];
	}
      
	gain_adc = x_peaks[1] - ped;
	ypeak = y_peaks[1];
      }
    }

    // Valley search
    if (npeaks >= 2) {
      Int_t binmin = hdata->GetXaxis()->FindBin( x_peaks[0] );
      Int_t binmax = hdata->GetXaxis()->FindBin( x_peaks[1] );
      yvalley = hdata->GetMaximum(); 
      for (Int_t bin = binmin; bin < binmax; bin++){
        if (hdata->GetBinContent(bin) < yvalley) { 
	  xvalley = bin;
          yvalley = hdata->GetBinContent(bin); 
	}
      }
      
      TList *functions = hdata->GetListOfFunctions(); // add valley marker to histogram
      TPolyMarker *pm = (TPolyMarker*)functions->FindObject("TPolyMarker");
      pm->SetNextPoint(xvalley, yvalley);
    }

    // Gain ADC
    gains_adc.add(gain_adc);
    if (run.type != DarkRun) { // check for dead channels if not dark run
      if (gain_adc == 0.) {
	std::cout << "[WARNING] processData(): PMT " << pmt << " has dead pixel " << ipx + 1 << std::endl;
      }
    }

    // Pedestal pos
    pedestal_pos.add(ped);

    // Peak-to-valley ratio
    peakvalley_ratios.add(ypeak / yvalley);

    // Signal fraction
    Float_t nSignals = hdata->Integral(0, hdata->FindBin(ped - dist_ped));
    nSignals += hdata->Integral(hdata->FindBin(ped + dist_ped), nBins + 1);
    Float_t frac = nSignals / nEvents;
    Float_t efrac = std::sqrt(frac * (1. - frac) / nEvents);
    signal_frac.add(frac);

    // Dark counts rate (kHz)
    dark_rates.add(frac / shaping_time / 1e3);
    dark_rate_errors.add(efrac / shaping_time / 1e3);

    // MAROC gain for each px
    Float_t ref_gain = (ADC_max - ped) / 3.; // 3p.e. peak at ADC max
    Float_t MAROCGain = gain_adc > 0. ? (ref_gain / gain_adc) * MAROCGain0 : 0.; 
    MAROCGains.add(MAROCGain);
  }//ipx

  // Gains
  Data gains = charge(m_MAROCGain[pmt], gains_adc);

  // Uniformity (use ADC gains)
  Float_t min = gains_adc.min();
  Float_t max = gains_adc.max();
  if (min == 0 && max > 0) { // try to compute uniformity skipping dead pixels
    min = +HUGE_VAL;
    for (const auto &val : gains_adc) if (val > 0 && val < min) min = val;
    if (min == +HUGE_VAL) min = 0.;
  } 
  Data uniformity = 100. * (min / max);

  // Dark current (nA)
  Data Q = 0.;
  if (m_datasheet.contains("Average Gain", pmt)) {
    Q = (m_datasheet["Average Gain"][pmt] * 1e6) * 1.6e-19; // Hmmm! I'm using datasheet gain!
  }
  Data dark_currents = Q * (dark_rates * 1e3) * 1e9;

  // Fill monitoring variables
  fillMoniVar(run, "Gain ADC", pmt, gains_adc);
  fillMoniVar(run, "Gain", pmt, gains);
  fillMoniVar(run, "Pedestal pos", pmt, pedestal_pos);
  fillMoniVar(run, "Signal fraction", pmt, signal_frac);
  fillMoniVar(run, "MAROC gains", pmt, MAROCGains);
  fillMoniVar(run, "Uniformity", pmt, uniformity);
  fillMoniVar(run, "Dark count rate", pmt, dark_rates);
  fillMoniVar(run, "Dark count rate error", pmt, dark_rate_errors);
  fillMoniVar(run, "Dark current", pmt, dark_currents);
  fillMoniVar(run, "Peak to valley ratio", pmt, peakvalley_ratios);
}


//////////////////
// Analyze data //
//////////////////
void OnlineMonitor::analyze() 
{
  TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1000, 800);
  TString pdfname = Form("%s/%s_monitor_analysis_summary_%s.pdf", 
      m_workDir.Data(), m_name.Data(), m_configName.Data());
  canv->SaveAs(pdfname + "[");

  // Get all runs we need
  std::vector<Run> dc_runs = runs(DarkRun);
  std::vector<Run> led_runs = runs(LEDRun);

  // Find run @1kV
  Run run1kV;
  for (const auto &run : led_runs) {
    if (run.hv == 1.) { 
      run1kV = run;
      break;
    }
  }

  // Analyze DCR over time
  if (dc_runs.size() > 0) {
    // Get times
    Data times; 
    for (const auto run : dc_runs) {
      TString fname = Form("%s/%i/feb-%s-000%i.mdf", 
          m_workDir.Data(), run.number, m_pmtMap[pmts()[0]].first.Data(), run.number);
      struct stat st; int ierr = stat (fname.Data(), &st);
      if (ierr == 0) times.add(Float_t(st.st_mtime));
    }
    Float_t time0 = times[0];
    for (auto &time : times) time = (time - time0) / 3600.;

    // Get DCR
    DataMap rates, erates; 
    for (const auto &pmt : pmts()) {
      for (const auto &run : dc_runs) {
        rates.add(pmt, m_data[run]["Total Dark count rate"][pmt]);
        erates.add(pmt, m_data[run]["Total Dark count rate error"][pmt]);
      }
    }

    // Plot DCR
    Range range = rates.range();
    if (m_tol.contains("Total Dark count rate")) range &= m_tol["Total Dark count rate"];
    int icol(0); 

    for (const auto pmt : pmts()) {
      if (icol == 0 || icol == 10 || icol == 19) icol++; // avoid white-ish colors
      TString opt = (pmt == pmts()[0]) ? "APL" : "PL same"; 
      TGraphErrors *gr = plotGraph(times, rates[pmt], {}, erates[pmt], "Dark count rate " + pmt, 
          "Dark count rate over time", "Hours", "Dark counts rate [kHz]", range, icol++, opt);
      gr->Write();
      plotTolerance("Total Dark count rate", gr);
    }

    canv->SaveAs(pdfname);
    canv->Clear();
    
    //plotMoniCorrel(dc_runs[0], "Average Gain", "Total Dark count rate", "[x10^{6}]", "[kHz]");
    //canv->SaveAs(pdfname);
    //canv->Clear();

    //plotMoniHisto(dc_runs[0], "Total Dark count rate", "DCR [kHz]", 100, {0., 100.}); 
    //canv->SaveAs(pdfname);
    //canv->Clear();
  }
  else {
    std::cout << "[WARNING] analyze(): No Dark current runs found!" << std::endl;
  }


  // Analyze 1kV LED run
  if (run1kV.number > 0) {
    std::cout << run1kV << std::endl; 
    
    canv->Divide(2, 2);
    canv->cd(1);
    plotMoniCorrel(run1kV, "Average Gain", "Average Gain", "[x10^{6}]", "[x10^{6}]");

    canv->cd(2);
    plotMoniCorrel(run1kV, "Average Gain", "Uniformity", "[x10^{6}]", "");

    canv->cd(3);
    plotMoniHisto(run1kV, "Average Gain", "Average Gain [x10^{6}]", 25, {0., 12.}); 

    canv->cd(4);
    plotMoniHisto(run1kV, "Uniformity", "Uniformity", 25, {0., 100.}); 
    canv->SaveAs(pdfname);
    canv->Clear();

    plotMoniHisto(run1kV, "Peak to valley ratio", "Peak to valley ratio", 25, {0., 12.});
    canv->SaveAs(pdfname);
    canv->Clear();
  }
  else {
    std::cout << "[ERROR] analyze(): No LED run @1kV found! Stop analysis!" << std::endl;
  }

  // Save canvas
  canv->SaveAs(pdfname + "]");
}

