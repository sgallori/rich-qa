#ifndef OFFLINE_MONITOR_H
#define OFFLINE_MONITOR_H

#include "Monitor.h"


//=================
// Offline monitor 
//=================
class OfflineMonitor: public Monitor
{		    
  public:
    OfflineMonitor(TString name = "OfflineMonitor") : Monitor(name) {};
    ~OfflineMonitor() {};

    // Plot graphs needed (user-defined)
    void makePlots(const Run run);

    // Process input data (user-defined)
    void processData(const Run run, const TString pmt);

    // Analyze data (user-defined)
    void analyze();

  private:
};
#endif



