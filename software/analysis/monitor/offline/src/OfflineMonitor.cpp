#include "OfflineMonitor.h"


////////////////
// Make plots //
////////////////
void OfflineMonitor::makePlots(const Run run)
{
  if (run.type == LEDRun) {
    TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1300, 1600);
    TString pdfname = Form("%s/%s_monitor_summary.pdf", m_runDir.Data(), m_name.Data());
    canv->SaveAs(pdfname + "[");
    
    // Fit-related plots
    canv->Divide(3, 4);
    canv->cd(1);
    plotBox("", true);

    canv->cd(2);
    plotMoniGraph(run, "Average Gain", "[x10^{6}]", kOrange, "APL");
    plotMoniGraph(run, "Min Gain", "[x10^{6}]", kRed, "PL same");
    plotMoniGraph(run, "Max Gain", "[x10^{6}]", kBlue, "PL same");

    canv->cd(3);
    plotMoniGraph(run, "Average Pedestal width", "[ADC]", kBlue, "APL");

    canv->cd(4);
    plotMoniGraph(run, "Average Signal width", "[ADC]", kBlue, "APL");

    canv->cd(5);
    plotMoniGraph(run, "Average Occupancy", "", kBlue, "APL");

    canv->cd(6);
    plotMoniGraph(run, "Average Crosstalk", "", kBlue, "APL");

    canv->cd(7);
    plotMoniGraph(run, "Average Missing probability", "[%]", kBlue, "APL");
    
    canv->cd(8);
    plotMoniGraph(run, "Fit failure rate", "", kBlue, "APL");

    canv->cd(9);
    plotMoniMap(run, "Fit status");

    canv->cd(10);
    plotMoniMap(run, "Fit chi2");

    canv->cd(11);
    plotMoniMap(run, "Occupancy");

    canv->cd(12);
    plotMoniMap(run, "Crosstalk");
    canv->SaveAs(pdfname);
    canv->Clear();

    // Other plots
    canv->Divide(1, 2);
    canv->cd(1);
    plotMoniCorrel(run, "Average Gain", "Average Gain", "[x10^{6}]", "[x10^{6}]");

    canv->cd(2);
    plotMoniCorrel(run, "Average Gain", "Uniformity", "[x10^{6}]", "");
    canv->SaveAs(pdfname);
    canv->Clear();

    plotMoniHisto(run, "Peak to valley ratio", "Peak to valley ratio");
    canv->SaveAs(pdfname);
    canv->Clear();

    // Save canvas
    canv->SaveAs(pdfname + "]");
  }
}


/////////////////////////////
// Process data of a board //
/////////////////////////////
void OfflineMonitor::processData(const Run run, const TString pmt)
{
  Data gains_adc(NPX), err_gains_adc(NPX);
  Data pedestal_widths(NPX), signal_widths(NPX);
  Data occupancies(NPX), crosstalks(NPX), pv_ratios(NPX);
  Int_t statuses[NPX]; Data fit_statuses(NPX);
  Data chi2(NPX), pmiss(NPX);

  if (run.type != LEDRun) {
    std::cout << "[WARNING] processData(): Processing of run type " 
      << run.type << " not defined yet! Skipping.." << std::endl;
    return;
  }

  // Open data file
  TString file = Form("%s/fit_%s.root", m_runDir.Data(), pmt.Data());
  if (gSystem->AccessPathName(file)) {
    std::cout << "[ERROR] processData(): File " << file << " does not exist!" << std::endl;
    abort();
  }
  TFile *rootfile = TFile::Open(file, "read");   
  TTree *tree = (TTree*)rootfile->Get("tree");

  // Set branches
  tree->SetBranchAddress("mean_1ph", gains_adc.ptr());
  tree->SetBranchAddress("sigma_1ph", signal_widths.ptr());
  tree->SetBranchAddress("sigma_n", pedestal_widths.ptr());
  tree->SetBranchAddress("navgph", occupancies.ptr());
  tree->SetBranchAddress("navgct", crosstalks.ptr());
  tree->SetBranchAddress("pv_ratio", pv_ratios.ptr());
  tree->SetBranchAddress("status", statuses);
  tree->SetBranchAddress("chi2", chi2.ptr());
  tree->SetBranchAddress("Pmiss", pmiss.ptr());
  tree->SetBranchAddress("err_mean_1ph", err_gains_adc.ptr());
  tree->GetEntry(0);

  // Gains
  Data gains = charge(m_MAROCGain[pmt], gains_adc);
  //Data err_gains = charge(m_MAROCGain[pmt], err_gains_adc); // gives negative values!!!
  Data err_gains = gains * (err_gains_adc / gains_adc);

  // Occupancy/cross-talk
  for (auto &val : occupancies) val = 1. - TMath::Exp(-val);
  for (auto &val : crosstalks) val = 1. - TMath::Exp(-val);

  // Fit failure rate
  Int_t nFailed = 0.;
  for (int ipx = 0; ipx < NPX; ipx++) {
    fit_statuses[ipx] = statuses[ipx];
    if (fit_statuses[ipx] != 0) nFailed++;
  }
  Data failure_rate = 100. * nFailed / Float_t(NPX);

  // Uniformity (use ADC gains)
  Float_t min = gains_adc.min();
  Float_t max = gains_adc.max();
  if (min == 0 && max > 0) { // try to compute uniformity skipping dead pixels
    min = +HUGE_VAL;
    for (const auto &val : gains_adc) if (val > 0 && val < min) min = val;
    if (min == +HUGE_VAL) min = 0.;
  } 
  Data uniformity = 100. * (min / max);

  // Add monitoring variables
  fillMoniVar(run, "Gain ADC", pmt, gains_adc);
  fillMoniVar(run, "Gain", pmt, gains);
  fillMoniVar(run, "Gain error", pmt, err_gains);
  fillMoniVar(run, "Signal width", pmt, signal_widths);
  fillMoniVar(run, "Pedestal width", pmt, pedestal_widths);
  fillMoniVar(run, "Occupancy", pmt, occupancies);
  fillMoniVar(run, "Crosstalk", pmt, crosstalks);
  fillMoniVar(run, "Peak to valley ratio", pmt, pv_ratios);
  fillMoniVar(run, "Uniformity", pmt, uniformity);
  fillMoniVar(run, "Fit failure rate", pmt, failure_rate);
  fillMoniVar(run, "Fit status", pmt, fit_statuses);
  fillMoniVar(run, "Fit chi2", pmt, chi2);
  fillMoniVar(run, "Missing probability", pmt, pmiss * 100.);
}


//////////////////
// Analyze data //
//////////////////
void OfflineMonitor::analyze() 
{
  // k-factor fitting function
  TF1 *kFun = new TF1( "kFun", "[0]*TMath::Power(x, [1]*12)", 0.85, 1.1 );
  kFun->SetParameters(10., 0.4);

  // Get LED runs
  std::vector<Run> led_runs = runs(LEDRun);
  if (led_runs.size() == 0) {
    std::cout << "[WARING] No LED runs found! Stop analysis!" << std::endl;
    return;
  }

  // Find run @1kV
  Run run1kV;
  for (const auto &run : led_runs) {
    if (run.hv == 1.) { 
      run1kV = run;
      break;
    }
  }
  if (run1kV.number == 0) {
    std::cout << "[ERROR] No LED run @1kV found! Stop analysis!" << std::endl;
    abort();
  }
  std::cout << run1kV << std::endl; 

  MoniMap &data = m_data[run1kV];
  std::vector<TGraph*> fit_graphs;

  // Loop over pmts
  for (const auto &pmt : pmts()) {
    // Get k-factor
    Data x, y, ex, ey; 
    for (const auto &run : led_runs) { 
      x.add( run.hv );
      y.add( m_data[run]["Average Gain"][pmt] );

      //Data err = m_data[run]["Gain error"][pmt]; // sum in quadrature
      //ey.add( std::sqrt((err*err).sum()) / err.size() );
    }

    TGraphErrors* gr = plotGraph(x, y, ex, ey, "Gain vs HV", "Gain vs HV " + pmt, "[kV]", "Me^{-}", {}, kBlack, "AP");
    gr->Fit(kFun, "", "g", 0.85, 1.1);
    Data fitted_gain = kFun->GetParameter(0);
    Data kfactor = kFun->GetParameter(1);
    fit_graphs.push_back(gr);

    // Get average gain
    Data avg_gain = data["Average Gain"][pmt];

    // Get uniformity
    Data uniformity = data["Uniformity"][pmt];

    // Get PV ratio
    Data pv_ratios = data["Peak to valley ratio"][pmt];

    // Fill analysis variables
    m_anaData.add("k-factor", pmt, kfactor);
    m_anaData.add("Average Gain", pmt, avg_gain);
    m_anaData.add("Uniformity", pmt, uniformity);
    m_anaData.add("Peak to valley ratio", pmt, pv_ratios);
  }//pmt


  // Plots analysis variables
  TCanvas *canv = new TCanvas("Canv_summary", "Monitor summary", 1000, 800);
  TString pdfname = Form("%s/%s_monitor_analysis_summary_%s.pdf", 
      m_workDir.Data(), m_name.Data(), m_configName.Data());
  canv->SaveAs(pdfname + "[");

  // k-factor fits
  canv->Divide(4, 4);
  int ipad = 0;
  for (auto &gr : fit_graphs) {
    canv->cd(++ipad);
    gr->SetMarkerSize(1);
    gr->Draw("APL");
  }
  canv->SaveAs(pdfname);
  canv->Clear();

  // Other variables
  canv->Divide(2, 2);
  canv->cd(1);
  plotAnaHisto("k-factor", "k-factor", 25, {0.3, 0.8}); 

  canv->cd(2);
  plotAnaHisto("Uniformity", "Uniformity", 25, {0., 100.}); 
  
  canv->cd(3);
  plotAnaHisto("Average Gain", " Average Gain [x10^{6}]", 25, {0., 12.}); 
  canv->SaveAs(pdfname);
  canv->Clear();
  
  canv->cd(4);
  plotAnaHisto("Peak to valley ratio", "Peak to valley ratio", 25, {0., 12.});
  canv->SaveAs(pdfname);
  canv->Clear();

  plotMoniCorrel(run1kV, "Average Gain", "Average Gain", "[x10^{6}]", "[x10^{6}]");
  canv->SaveAs(pdfname);
  canv->Clear();

  plotMoniCorrel(run1kV, "Average Gain", "Uniformity", "[x10^{6}]", "");
  canv->SaveAs(pdfname);
  canv->Clear();

  // Save canvas
  canv->SaveAs(pdfname + "]");
}


