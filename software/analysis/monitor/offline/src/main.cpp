#include "OfflineMonitor.h"

//========================
// Run Offline Monitor 
//========================
int main(int argc, char **argv)
{
  Int_t run = 0;
  TString runinfo = "";
  TString configfile(argv[1]);
  if (argc >= 3) { 
    run = atoi(argv[2]); 
    for (int i = 3; i < argc; i++) 
      runinfo += TString(argv[i]) + " "; 
  }

  // The monitor
  Monitor *moni = new OfflineMonitor("Offline");
  
  // Read config file
  moni->readConf(configfile);
  //moni->printDatasheet();

  // Define tolerances
  moni->setTolerance("Average Gain", 1., HUGE_VAL); // Me-
  moni->setTolerance("Uniformity", 25., 100.); 
  moni->setTolerance("Dark current", 0., 4.); // nA
  moni->setTolerance("Dark count rate", 0., 16.); // kHz
  moni->setTolerance("Peak to valley ratio", 1.3, HUGE_VAL); 
  moni->setTolerance("k-factor", 0., HUGE_VAL);
  moni->setTolerance("Fit failure rate", 0., 10.);
  moni->setTolerance("Fit chi2", 0., 30.);
  moni->setTolerance("Fit status", -1., 1.);
  
  // Run monitor!
  if (run) {
    moni->deleteRuns(); // remove read runs
    moni->addRun(run, runinfo);
  }
  moni->run();

  return 0;
}
