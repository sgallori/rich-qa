#!/bin/bash

# Data dir (to be passed from outside)
DATA_HOME='/home/lhcb/pdqa/Data/'

usage() { echo "Usage: $0 [-p PMT1,PMT2,..] [-r RUN1,RUN2,..]" 1>&2; exit 1; }


# Get external arguments
while getopts ":p:r:" opt; do
    case "${opt}" in
        p)
	  set -f
	  IFS=','
	  PMTS=($OPTARG);;
          #PMTS+=("$OPTARG")
        r)
	  set -f
	  IFS=','
	  RUNS=($OPTARG);;
        *)
	  usage;;
    esac
done
shift $((OPTIND-1))

if [ -z "${PMTS}" ] || [ -z "${RUNS}" ]; then
    usage
fi

echo Number of runs to be processed: ${#RUNS[@]} 
for run in ${RUNS[@]}; do echo $run; done

echo Number of PMTs to be processed: ${#PMTS[@]} 
for run in ${PMTS[@]}; do echo $run; done

# Loop over runs
for run in ${RUNS[@]}; do
  # Loop over PMTs
  for pmt in ${PMTS[@]}; do
     # Run fit
     rundir=$DATA_HOME/$run
     ./bin/fit $pmt $rundir/$pmt"_000"$run.root $rundir &> $rundir/fit_$pmt.log &
     #echo "./bin/fit $pmt $rundir/$pmt"_000"$run.root $rundir &> $rundir/fit_$pmt.log"
  done
  wait # wait until all PMTs are done
done
