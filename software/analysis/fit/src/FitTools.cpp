#include "Fit.h"

////////////////
// Do the fit //
////////////////
void Fit::fit(const int pxID)
{
  // Fit configuration
  // Do not fit (overlays model to data using initial parameters)
  const bool dryrun = false;

  // peaks finding
  const Float_t thr = 1e-3; // find peaks w/ height >= thr*max_height 	
  const Float_t sigma = 3.; // separation between peaks 

  // max. number of photons
  const int Nphotons = 2;
  
  // max. number of cross-talk signals
  int Ncrosstalks = 2;

  // max. number of missing dynodes
  int Nmissed = 6;  

  // Param. for modelling conversion on dynodes
  // Gi = G**1/N * (ri**k / (r1*r2)**k/N) = G**1/N * G0
  const Int_t Ndynodes = 12;
  const Double_t r1 = 2.3;
  const Double_t r2 = 1.2;
  const Double_t k = 2./3.;
  const Double_t pcorr = 0.3;
  const Double_t dADCdQ = 7.e-5;   // Q -> ADC conversion factor 
  const Double_t corr_fact = 0.45; // corr. factor wrt dynode conversion model

  // min crosstalk probability and significance wrt noise (if less, fix cross-talk to zero)
  const Double_t navgct_min = 0.01; 
  const int significance_ct_min = 3;

  // min. value of 1pe peak wrt to noise (if less, fix Pmiss to zero)
  const Double_t mean_1ph_min = 20.;

  std::cout << std::endl;
  std::cout << "[INFO] ======================================================" << std::endl;
  std::cout << "[INFO] Processing MaPMT: " << m_pmt << ", pixel: " << pxID     << std::endl;
  std::cout << "[INFO] ======================================================" << std::endl;

  gROOT->cd();

  TCanvas* cFit0 = new TCanvas("cFit0", Form("Initial fit for px %i", pxID), 1000, 500);
  TCanvas* cFit = new TCanvas("cFit", Form("Fit to spectrum of pixel %i", pxID), 1000, 500);

  TH1F *hdata;
  Int_t nEvents = -1;
  TString prefix = outputPrefix();
  bool only_one_px = prefix.Contains("px");


  //==================
  // Load MAROC data
  //==================
  // Set chain
  TChain ch("mdfTree");
  ch.Add(m_inputFile);
  nEvents = ch.GetEntries(); 

  // ADC for pixels
  TString branchName = "B01_PMT1_ADC"; // ATTENZIONE!!!! E' solo PMT1!!!!
  UShort_t PMT_ADC[NPX]; ch.SetBranchAddress(branchName, &PMT_ADC[0]);

  // Set data histogram
  Double_t x_min = Int_t(0.1 * ch.GetMinimum(branchName));  // far away to avoid periodic behaviour of FFT...
  Double_t x_max = ch.GetMaximum(branchName); // max ADC value among all pixels
  if (x_max > 256.) x_max = 256.;
  if (x_min < 0.) x_min = 0.;
  Int_t nBins = x_max - x_min;
  hdata = new TH1F("hdata", "", nBins, x_min, x_max);
  hdata->SetMinimum(1.);
  std::cout << "[INFO] Data histogram binning: "
   << nBins << " bins between [" << x_min << ", " << x_max << "]" << std::endl;  

  // Event loop
  for (Int_t i = 0; i < nEvents; i++) {
    ch.GetEvent(i); 
    hdata->Fill( Float_t(PMT_ADC[pxID-1]) ); 
  }//evts
  std::cout << "[INFO] Number of events: " << nEvents << std::endl;

  
  //======================
  // Define RooDataset
  //======================
  RooRealVar x("ADC", "ADC", x_min, x_max);
  x.setBins(hdata->GetNbinsX(), "cache"); // nBins too aggressive?
  RooDataHist dh("dh", "dh", x, RooFit::Import(*hdata));


  //=================
  // Peaks finding
  //=================
  std::cout << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;
  std::cout << "[INFO] Searching peaks..."                 << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;
  TCanvas *cPeaks = new TCanvas("cPeaks", "Peaks finding", 600, 600); 
  cPeaks->SetLogy();
 
  Double_t x_sig_peak = 0., y_sig_peak = 0., y_valley = 0., x_valley = 0., pv_ratio = 0.;
  TSpectrum *s = new TSpectrum(10);
  Int_t npeaks = s->Search(hdata, sigma, "nobackground", thr); 
 
  //TH1F* hsmooth = (TH1F*)hdata->Clone("hsmooth"); // smooth the histogram!
  //hsmooth->Smooth(); 
  //npeaks = s->Search(hsmooth, sigma, "nobackground", thr); // use smoothed histogram!

#if ROOT_VERSION_CODE >= ROOT_VERSION(6,00,0)
  Double_t *x_peaks = s->GetPositionX(); // ROOT6 needs Double_t!!!
  Double_t *y_peaks = s->GetPositionY();
#else
  Float_t *x_peaks = s->GetPositionX(); // ROOT6 needs Double_t!!!
  Float_t *y_peaks = s->GetPositionY();
#endif

  // Valley search
  if (npeaks >= 2) {
    x_sig_peak = x_peaks[1];
    y_sig_peak = y_peaks[1];
    Int_t binmin = hdata->GetXaxis()->FindBin( x_peaks[0] );
    Int_t binmax = hdata->GetXaxis()->FindBin( x_peaks[1] );
    y_valley = hdata->GetMaximum(); 
    for (Int_t bin = binmin; bin < binmax; bin++) {
      if (hdata->GetBinContent(bin) < y_valley) { 
	x_valley = bin;
	y_valley = hdata->GetBinContent(bin); 
      }
    }
    pv_ratio = y_sig_peak / y_valley;
  }

  if (npeaks < 2) {
    std::cout << "[ERROR] Just 1 peak found! Skip!" << std::endl;
    m_status[pxID -1] = -1;
    if (pxID == NPX) cFit0->SaveAs(m_outputDir + "/raw" + prefix + ".pdf]"); 
    if (pxID == NPX) cFit->SaveAs(m_outputDir + "/" + prefix + ".pdf]"); 
    return;
  } 

  if (x_peaks[0] > x_peaks[1]) {
    std::cout << "[ERROR] Noise position lower than signal position! Skip!" << std::endl;
    s->Print();
    if (pxID == NPX) cFit0->SaveAs(m_outputDir + "/raw" + prefix + ".pdf]"); 
    if (pxID == NPX) cFit->SaveAs(m_outputDir + "/" + prefix + ".pdf]"); 
    return;
  }
  
  std::cout << "[INFO] Found " << npeaks << " candidate peaks !" << std::endl;
  std::cout << "[INFO] Peak valley ratio " << pv_ratio << std::endl;
  s->Print();

  
  //===========================
  // Estimate starting params
  //===========================
  std::cout << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;
  std::cout << "[INFO] Estimating initial parameters..."   << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;
  Double_t ped_pos = x_peaks[0];
  Double_t sig_pos = x_peaks[1];
  Double_t sigma_n_min = 1. / 2.3; // FWHM (1ADC) / 2.3
  Double_t gain_adc = sig_pos - ped_pos;
  Double_t ct_pos = 0.05 * gain_adc; // Cross-talk is about 5% of signal peak

  // noise pdf
  RooRealVar mean_n0("mean_n0", "Initial mean of noise", ped_pos, 0.95*ped_pos, 1.05*ped_pos);
  RooRealVar sigma_n0("sigma_n0", "Initial width of noise", 1.5*sigma_n_min, sigma_n_min, 2.*sigma_n_min); 
  RooGaussian noise_pdf0("noise_pdf0", "Initial noise pdf", x, mean_n0, sigma_n0); 

  // cross talk pdf
  RooRealVar mean_1ct0("mean_1ct0", "Initial mean of crosstalk", ct_pos, 0.5*ct_pos, 1.5*ct_pos); 
  RooRealVar sigma_1ct0("sigma_1ct0", "Initial width of cross-talk", ct_pos, 0.5*ct_pos, 5.*ct_pos); // Find better way for these values!!! 
  RooRealVar navgct0("navgct0", "Initial mean number of cross-talks", 0.2, 0., 1.); 
  RooFormulaVar mean_1ct_tot("mean_1ct_tot", "mean_n0 + mean_1ct0", RooArgList(mean_n0, mean_1ct0));
  RooFormulaVar sigma_1ct_tot("sigma_1ct_tot", "TMath::Sqrt(TMath::Power(sigma_n0,2) + TMath::Power(sigma_1ct0,2))", RooArgList(sigma_n0, sigma_1ct0));
  RooGaussian crosstalk_pdf0("crosstalk_pdf0", "Initial cross talk pdf", x, mean_1ct_tot, sigma_1ct_tot); 

  // 1pe signal pdf
  RooRealVar mean_1ph0("mean_1ph0", "Initial mean of 1pe peak", gain_adc, 0.5*gain_adc, 1.5*gain_adc);
  RooRealVar sigma_1ph0("sigma_1ph0", "Initial width of 1pe peak", 1.5*sqrt(gain_adc), sqrt(gain_adc), 3.*sqrt(gain_adc)); // #### WARNING: it was 2!! ###
  RooFormulaVar mean_1ph_tot("mean_1ph_tot", "mean_n0 + mean_1ph0", RooArgList(mean_n0, mean_1ph0));
  RooFormulaVar sigma_1ph_tot("sigma_1ph_tot", "TMath::Sqrt(TMath::Power(sigma_n0,2) + TMath::Power(sigma_1ph0,2))", RooArgList(sigma_n0, sigma_1ph0));
  RooGaussian signal_pdf0("signal_pdf0", "Initial 1pe signal pdf", x, mean_1ph_tot, sigma_1ph_tot); 

  // Build initial pedestal model to estimate noise and crosstalk parameters
  RooFormulaVar norm_ct0("norm_ct0", "1. - TMath::Poisson(0, navgct0)", RooArgList(navgct0));
  RooAddPdf pedestal_pdf0("pedestal_pdf0", "Initial pedestal model", RooArgList(crosstalk_pdf0, noise_pdf0), RooArgList(norm_ct0));

  // Fit pedestal with noise + crosstalk!
  x.setRange("Ped", 0.7*ped_pos, 1.3*ped_pos);
  x.setRange("Sig", 0.7*sig_pos, 1.3*sig_pos);
  RooFitResult *fitped0 = pedestal_pdf0.fitTo(dh, RooFit::Save(true), RooFit::Range("Ped"));

  // Fit signal!
  mean_n0.setConstant(true);
  sigma_n0.setConstant(true);
  RooFitResult *fitsig0 = signal_pdf0.fitTo(dh, RooFit::Save(true), RooFit::Range("Sig"));

  // Plot components!
  cFit0->cd();
  RooPlot* frame0 = x.frame( RooFit::Title(Form("pixel %i", pxID)) );
  frame0->SetName( Form("frame0_px%i", pxID) );

  dh.plotOn( frame0 );
  pedestal_pdf0.plotOn(frame0, "", RooFit::LineColor(kBlue), RooFit::Range("Full"));
  pedestal_pdf0.plotOn(frame0, RooFit::Components("noise_pdf0"), RooFit::LineColor(kBlack), RooFit::LineStyle(kSolid), RooFit::Range("Full")); 
  pedestal_pdf0.plotOn(frame0, RooFit::Components("crosstalk_pdf0"), RooFit::LineColor(kRed), RooFit::LineStyle(kDashed), RooFit::Range("Full")); 
  signal_pdf0.plotOn(frame0, "", RooFit::LineColor(kBlue), RooFit::Range("Full"));

  cFit0->SetLogy();
  frame0->SetMinimum( 1. );
  frame0->Draw();
  
  TList *functions = hdata->GetListOfFunctions(); // draw polymarker
  TPolyMarker *pm = (TPolyMarker*)functions->FindObject("TPolyMarker");
  pm->SetNextPoint(x_valley, y_valley); // add valley marker
  pm->Draw("same");

  TLatex caption; // captions
  caption.SetTextSize(0.04);
  caption.DrawLatex(0.8*x_max, 1.2*y_peaks[1], Form("Pixel %i", pxID));

  // Dump canvas on .pdf
  if (only_one_px) {
    cFit0->SaveAs(m_outputDir + "/raw" + prefix + ".pdf");
  }
  else {
    if (pxID == 1) cFit0->SaveAs(m_outputDir + "/raw" + prefix + ".pdf[");
    if (pxID >= 1 && pxID <= NPX) cFit0->SaveAs(m_outputDir + "/raw" + prefix + ".pdf");
    if (pxID == NPX) cFit0->SaveAs(m_outputDir + "/raw" + prefix + ".pdf]"); 
  } 


  //=================================
  // Check on estimated parameters
  //=================================
  // Estimate average num. photons
  Double_t Nevt = hdata->GetEntries();
  Double_t Nevt_ped = hdata->Integral(hdata->FindBin(0.7*ped_pos), hdata->FindBin(1.3*ped_pos)); 
  RooRealVar navgph0("navgph0", "Initial mean number of photons", -log(Nevt_ped / Nevt)); 
  
  // Estimate missing probability (empirical formula found on data)
  RooRealVar Pmiss0("Pmiss0", "Initial prob. missing a dynode", 0.00247*pow(pv_ratio, 2) - 0.044*pv_ratio + 0.439); 

  // Check if cross-talk can be distinguished from noise... if not... set it to zero!
  if (ct_pos < significance_ct_min * sigma_n_min || navgct0.getVal() < navgct_min) { 
    std::cout << std::endl;
    std::cout << "[INFO] Cross-talk estimated to be too small or close to pedestal. Fixed to zero!" << std::endl;

    mean_1ct0.setVal(0.); 
    mean_1ct0.setConstant(true); 
    sigma_1ct0.setVal(0.); 
    sigma_1ct0.setConstant(true); 
    navgct0.setVal(0.); 
    navgct0.setConstant(true); 
    Ncrosstalks = 0; 
  }

  // Check for smallness of Pmiss
  if (mean_1ph0.getVal() < mean_1ph_min) {
    std::cout << "[INFO] 1ph peak very close to pedestal. Fixed to zero!" << std::endl;
    std::cout << std::endl;

    Pmiss0.setVal(0.0);
    Pmiss0.setConstant(true);
    Nmissed = 0;
  }


  //=========================
  // Define fit parameters
  //=========================
  // mean and sigma of noise
  RooRealVar mean_n("mean_n", "mean of noise gaussians", mean_n0.getVal(), 0.95*mean_n0.getVal(), 1.05*mean_n0.getVal());
  RooRealVar sigma_n("sigma_n", "width of noise gaussians", sigma_n0.getVal(), sigma_n_min, 1.05*sigma_n0.getVal()); 

  // mean and sigma of single photon response
  RooRealVar mean_1ph("mean_1ph", "mean of single photon gaussian", mean_1ph0.getVal(), 0.5*mean_1ph0.getVal(), 1.5*mean_1ph0.getVal());
  RooRealVar sigma_1ph("sigma_1ph", "width of single photon gaussian", sigma_1ph0.getVal(), 0.5*sigma_1ph0.getVal(), 2.*sigma_1ph0.getVal()); 

  // average number of incident photons on pixel
  RooRealVar navgph("navgph", "mean number of incident photons per pixel", navgph0.getVal(), 0.5*navgph0.getVal(), 2.*navgph0.getVal());

  // mean and sigma of 1 p.e. component for cross talk
  RooRealVar mean_1ct("mean_1ct", "mean of 1pe cross-talk gaussian", mean_1ct0.getVal(), 0.5*mean_1ct0.getVal(), 2.*mean_1ct0.getVal()); 
  RooRealVar sigma_1ct("sigma_1ct", "width of 1pe cross-talk gaussian", sigma_1ct0.getVal(), 0.5*sigma_1ct0.getVal(), 2.*sigma_1ct0.getVal()); 

  // mean number of cross talk signals per pixels
  RooRealVar navgct("navgct", "mean number of cross-talk photons per pixel", navgct0.getVal(), 0., 2.*navgct0.getVal()); 

  // Total probability to miss a stage
  RooRealVar Pmiss("Pmiss", "Prob. missing a dynode", Pmiss0.getVal(), 0., 2.*Pmiss0.getVal()); 
  
  // Fix some parameters to initial estimate
  mean_n.setConstant(true);
  sigma_n.setConstant(true);
  mean_1ct.setConstant(true);
  sigma_1ct.setConstant(true);

  if (Nmissed == 0) { Pmiss.setVal(0.); Pmiss.setConstant(true); }
  if (Ncrosstalks == 0) { navgct.setVal(0.); navgct.setConstant(true); }


  //=====================
  // Build fit model
  //=====================
  std::cout << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;
  std::cout << "[INFO] Build fit model!                  " << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;


  //=====================
  // Build noise model
  //=====================
  RooAbsPdf *noise_pdf = new RooGaussian("noise_pdf", "Noise pdf", x, mean_n, sigma_n); 


  //======================
  // Build signal model
  //======================
  TString name, expr, title;
  RooArgList *pdfs = new RooArgList;
  RooArgList *coeffs = new RooArgList;
  RooAbsPdf *signal_pdf[Nphotons];
  Double_t G0 = 1.0;

  // Build single photon response
  for (int nmiss = 0; nmiss <= Nmissed; nmiss++) {
    // Gain reduction factor (g1*g2*g3*...)
    if (nmiss == 1) G0 *= corr_fact * pow(r1, k) / pow(r1*r2, k/Ndynodes);
    if (nmiss == 2) G0 *= corr_fact * pow(r2, k) / pow(r1*r2, k/Ndynodes);
    if (nmiss > 2)  G0 *= corr_fact / pow(r1*r2, k/Ndynodes);

    name.Form("Gmiss_dy%i", nmiss);
    expr.Form("%f * TMath::Power(mean_1ph / %f, %f)", G0, dADCdQ, Double_t(nmiss) / Ndynodes);
    RooFormulaVar *Gmiss = new RooFormulaVar(name, expr, RooArgList(mean_1ph)); 

    // mean
    name.Form("mean_1ph_dy%i", nmiss);
    expr.Form("mean_1ph / %s", Gmiss->GetName());
    RooFormulaVar *mean = new RooFormulaVar(name, expr, RooArgList(mean_1ph, *Gmiss));

    // sigma
    name.Form("sigma_1ph_dy%i", nmiss);
    expr.Form("sigma_1ph / TMath::Sqrt(%s)", Gmiss->GetName());
    RooFormulaVar *sigma = new RooFormulaVar(name, expr, RooArgList(sigma_1ph, *Gmiss));

    // gaussian 
    name.Form("pdf_1ph_dy%i", nmiss);
    title.Form("Gaussian for %i missing dynodes", nmiss);
    RooAbsPdf *gauss = new RooGaussian(name, title, x, *mean, *sigma); 

    // coefficient
    name.Form("N_1ph_dy%i", nmiss); 
    if (nmiss == 0) expr.Form("(1. - Pmiss)"); 
    if (nmiss == 1) expr.Form("Pmiss / (1. + %i*%f)", Nmissed-1, pcorr); 
    if (nmiss > 1)  expr.Form("%f * Pmiss / (1. + %i*%f)", pcorr, Nmissed-1, pcorr); 

    RooFormulaVar *coeff = new RooFormulaVar(name, expr, RooArgList(Pmiss));
    pdfs->add( *gauss );
    coeffs->add( *coeff );

    std::cout << "[INFO] Added signal gaussian " << gauss->GetName() 
      << " w/ gain reduction: " << Gmiss->getVal() << std::endl;
  }

  // Build pdf for multiple photons
  for (int nph = 1; nph <= Nphotons; nph++) {
    name.Form("signal0_pdf_%i", nph);
    title.Form("%i photons signal pdf", nph);
    if (nph == 1) signal_pdf[0] = new RooAddPdf(name, title, *pdfs, *coeffs);
    else signal_pdf[nph-1] = new RooFFTConvPdf(name, title, x, *(signal_pdf[nph-2]), *(signal_pdf[0]));
  }

  // add some noise :-)
  for (int nph = 1; nph <= Nphotons; nph++) {
    name.Form("signal_pdf_%i", nph);
    title.Form("%i photons signal pdf", nph);
    signal_pdf[nph-1] = new RooFFTConvPdf(name, title, x, *(signal_pdf[nph-1]), *noise_pdf);
  }
  

  //==========================
  // Build cross-talk model
  //==========================
  RooAbsPdf *crosstalk_pdf[Ncrosstalks];
  RooAbsPdf *crosstalk0_pdf[Ncrosstalks];

  for (int nct = 1; nct <= Ncrosstalks; nct++) {
    // mean
    name.Form("mean_ct_%i", nct);
    expr.Form("mean_n + %i*mean_1ct", nct);
    RooFormulaVar *mean = new RooFormulaVar(name, expr, RooArgList(mean_n, mean_1ct));

    // sigma
    name.Form("sigma_ct_%i", nct);
    expr.Form("TMath::Sqrt(sigma_n*sigma_n + %i*sigma_1ct*sigma_1ct)", nct);
    RooFormulaVar *sigma = new RooFormulaVar(name, expr, RooArgList(sigma_n, sigma_1ct));

    // gaussian 
    name.Form("crosstalk_pdf_%i", nct);
    title.Form("%i cross-talks pdf", nct);
    crosstalk_pdf[nct-1] = new RooGaussian(name, title, x, *mean, *sigma); 
    
    //========= no noise =========
    // mean
    name.Form("mean0_ct_%i", nct);
    expr.Form("%i*mean_1ct", nct);
    mean = new RooFormulaVar(name, expr, RooArgList(mean_1ct));

    // sigma
    name.Form("sigma0_ct_%i", nct);
    expr.Form("TMath::Sqrt(%i)*sigma_1ct", nct);
    sigma = new RooFormulaVar(name, expr, RooArgList(sigma_1ct));

    // gaussian 
    name.Form("crosstalk0_pdf_%i", nct);
    title.Form("%i cross-talks pdf", nct);
    crosstalk0_pdf[nct-1] = new RooGaussian(name, title, x, *mean, *sigma); 
    //============================
  }


  //====================
  // Build full model 
  //====================
  RooArgList *model_pdfs = new RooArgList;
  RooArgList *model_coeffs = new RooArgList;
  RooAbsPdf *signal_crosstalk_pdf[Nphotons][Ncrosstalks];

  for (int nph = 0; nph <= Nphotons; nph++) {
    for (int nct = 0; nct <= Ncrosstalks; nct++) {
      RooAbsPdf *pdf;
      name.Form("pdf_%iph_%ict", nph, nct);
      title.Form("%i photons + %i cross-talks pdf", nph, nct);

      // noise only
      if (!nph && !nct) pdf = noise_pdf;

      // signal only
      else if (nph && !nct) pdf = signal_pdf[nph-1];

      // cross-talk only
      else if (!nph && nct) pdf = crosstalk_pdf[nct-1];

      // signal + cross-talk
      else {
	pdf = new RooFFTConvPdf(name, title, x, *(signal_pdf[nph-1]), *(crosstalk0_pdf[nct-1]));
	signal_crosstalk_pdf[nph-1][nct-1] = pdf;
      }

      // normalization
      name.Form("coeff_%iph_%ict", nph, nct); 
      expr.Form("TMath::Poisson(%i, navgph) * TMath::Poisson(%i, navgct)", nph, nct); 
      RooFormulaVar *coeff = new RooFormulaVar(name, expr, RooArgList(navgph, navgct));

      // add to the model
      std::cout << "[INFO] Added component to model: " << name << std::endl;
      model_pdfs->add( *pdf );
      model_coeffs->add( *coeff );
    }
  }

  RooAddPdf model("model", "Spectrum model", *model_pdfs, *model_coeffs);
  //model.Print("v");


  //================
  // Fit spectrum
  //================
  std::cout << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;
  std::cout << "[INFO] Fit spectrum!"                      << std::endl;
  std::cout << "[INFO] ==================================" << std::endl;
  RooFitResult *fitres = nullptr;
  if (!dryrun) {
    fitres = model.fitTo(dh, RooFit::Save(true));
    fitres->SetName( Form("fit_result_px%i", pxID) );
    //const_params = model.getParameters(RooArgSet(x))->selectByAttrib("Constant", kTRUE); 
  }


  //==============================
  // Print and plot fit results
  //==============================
  // Plot components
  cFit->cd();
  RooPlot* frame = x.frame( RooFit::Title(Form("pixel %i", pxID)) );
  frame->SetName( Form("frame_px%i", pxID) );
  dh.plotOn( frame );
  model.plotOn(frame, "", RooFit::LineColor(kBlue)); // full model

  Double_t norm = TMath::Poisson(0, navgph.getVal()) * TMath::Poisson(0, navgct.getVal()); // noise
  noise_pdf->plotOn(frame, RooFit::Components("noise_pdf"), RooFit::LineColor(kBlack), RooFit::LineStyle(kSolid), RooFit::Normalization(norm)); 

  for (int nph = 1; nph <= Nphotons; nph++) { // signal
    Double_t norm = TMath::Poisson(nph, navgph.getVal()) * TMath::Poisson(0, navgct.getVal());
    signal_pdf[nph-1]->plotOn(frame, "", RooFit::LineColor(nph+1), RooFit::LineStyle(kSolid), RooFit::Normalization(norm));
  }

  for (int nct = 1; nct <= Ncrosstalks; nct++) { // cross-talk
    Double_t norm = TMath::Poisson(0, navgph.getVal())* TMath::Poisson(nct, navgct.getVal());
    crosstalk_pdf[nct-1]->plotOn(frame, "", RooFit::LineColor(nct+1), RooFit::LineStyle(kDashed), RooFit::Normalization(norm));
  }

  for (int nph = 1; nph <= Nphotons; nph++) { // signal + cross-talk
    for (int nct = 1; nct <= Ncrosstalks; nct++) {
      Double_t norm = TMath::Poisson(nph, navgph.getVal())* TMath::Poisson(nct, navgct.getVal());
      signal_crosstalk_pdf[nph-1][nct-1]->plotOn(frame, "", RooFit::LineColor(nph+1), RooFit::LineStyle(kDashed), RooFit::Normalization(norm));
    }
  }

  //cFit->SetLogy();
  frame->SetMinimum( 1. );
  frame->SetMaximum( 1.5*y_peaks[1] );
  frame->Draw();

  caption.SetTextSize(0.04);
  caption.DrawLatex(0.8*x_max, 1.2*y_peaks[1], Form("Pixel %i", pxID));

  // Dump canvas on .pdf
  if (only_one_px) {
    cFit->SaveAs(m_outputDir + "/" + prefix + ".pdf");
  }
  else {
    if (pxID == 1) cFit->SaveAs(m_outputDir + "/" + prefix + ".pdf[");
    if (pxID >= 1 && pxID <= NPX) cFit->SaveAs(m_outputDir + "/" + prefix + ".pdf");
    if (pxID == NPX) cFit->SaveAs(m_outputDir + "/" + prefix + ".pdf]"); 
  }


  // Print fitted values
  if (!dryrun) {
    std::cout << std::endl;
    std::cout << "[INFO] ============================================" << std::endl;
    std::cout << "[INFO]  Fit summary for PMT: " << m_pmt << ", pixel: " << pxID << std::endl;
    std::cout << "[INFO] ============================================" << std::endl;

    std::cout << "[INFO] ==================================" << std::endl;
    std::cout << "[INFO] Initial pedestal fit result"        << std::endl;
    std::cout << "[INFO] ==================================" << std::endl;
    fitped0->Print("v"); 
    
    std::cout << "[INFO] ==================================" << std::endl;
    std::cout << "[INFO] Initial signal fit result"          << std::endl;
    std::cout << "[INFO] ==================================" << std::endl;
    fitsig0->Print("v"); 
    
    std::cout << "[INFO] ==================================" << std::endl;
    std::cout << "[INFO] Final spectrum fit result"          << std::endl;
    std::cout << "[INFO] ==================================" << std::endl;
    fitres->Print("v"); 
  }

  // Print difference wrt initial values
  std::cout << std::endl;
  std::cout << "[INFO] Fitted values wrt initial estimation: " << std::endl;
  for (int ipar = 0; ipar < fitres->floatParsFinal().getSize(); ipar++) {
    const RooRealVar var = (const RooRealVar&)fitres->floatParsFinal()[ipar];
    const RooRealVar var0 = (const RooRealVar&)fitres->floatParsInit()[ipar];
    if (var.isConstant() == true) continue;
    std::cout << "[INFO] " << var.GetName() <<  " = " << 100.*(var.getVal() - var0.getVal()) / var0.getVal() << "%" << std::endl;  
  }
  std::cout << std::endl;

  // Hand-made chi2 computation
  Int_t ndf(0);
  Double_t chi2(0.);
  RooHist* histo = (RooHist*) frame->getHist("h_dh") ;
  RooCurve* curve = (RooCurve*) frame->getCurve("model_Norm[ADC]") ;

  for (Int_t i = 0; i < histo->GetN(); i++) {
    Double_t x, Ndata;
    histo->GetPoint(i, x, Ndata);
    Double_t sigma_ped = sqrt(pow(sigma_n.getVal(), 2) + pow(sigma_1ct.getVal(), 2));
    Double_t mean_ped = mean_n.getVal() + mean_1ct.getVal();
    if (x < mean_ped + 5.*sigma_ped) continue; // Exclude pedestal!
    
    if (Ndata > 0) { 
      Double_t Nfit = curve->interpolate(x);
      chi2 += pow(Ndata - Nfit, 2) / Ndata;
      //Double_t tmp = pow(Ndata - Nfit, 2) / Ndata;
      //std::cout << i << " " << x << " " << Ndata << " " << Nfit << " " << tmp << std::endl;
      ndf++;
    }
  }
  if (!dryrun) ndf -= fitres->floatParsFinal().getSize();
  chi2 /= ndf;

  // Check fit status
  Int_t status = 0;
  const Double_t threshold = 1e-3; // == 0.1 %
  const Double_t chi2Max = 20.;

  for (int ipar = 0; ipar < fitres->floatParsFinal().getSize(); ipar++) { // check if fit went to limit
    const RooRealVar var = (const RooRealVar&)fitres->floatParsFinal()[ipar];
    const RooRealVar var0 = (const RooRealVar&)fitres->floatParsInit()[ipar];
    const Double_t val = var.getVal(), min = var.getMin(), max = var.getMax();
    if (std::abs(val/min -1.) < threshold || std::abs(val/max -1.) < threshold) status++;
    //if (std::abs(val/val0 -1.) < threshold2) status++;
  }
  if (chi2 >= chi2Max) status += 100; // catch bad fit.. 

  std::cout << "[INFO] Goodness of the fit:" << std::endl; 
  std::cout << "[INFO] Ndf = " << ndf << ", chi2/ndf = " << chi2 << std::endl;
  std::cout << "[INFO] Fit status = " << status << std::endl;


  //==============
  // Fill NTuple 
  //==============
  m_chi2[pxID -1] = chi2;
  m_status[pxID -1] = status;

  m_navgph[pxID -1] = navgph.getVal();
  m_err_navgph[pxID -1] = navgph.getPropagatedError(*fitres);
  m_dnavgph[pxID -1] = navgph.getVal() - navgph0.getVal();

  m_navgct[pxID -1] = navgct.getVal();
  m_err_navgct[pxID -1] = navgct.getPropagatedError(*fitres);
  m_dnavgct[pxID -1] = navgct.getVal() - navgct0.getVal();
  
  m_mean_n[pxID -1] = mean_n.getVal();
  m_err_mean_n[pxID -1] = mean_n.getPropagatedError(*fitres);
  m_dmean_n[pxID -1] = mean_n.getVal() - mean_n0.getVal();
  
  m_sigma_n[pxID -1] = sigma_n.getVal();
  m_err_sigma_n[pxID -1] = sigma_n.getPropagatedError(*fitres);
  m_dsigma_n[pxID -1] = sigma_n.getVal() - sigma_n0.getVal();

  m_mean_1ph[pxID -1] = mean_1ph.getVal();
  m_err_mean_1ph[pxID -1] = mean_1ph.getPropagatedError(*fitres);
  m_dmean_1ph[pxID -1] = mean_1ph.getVal() - mean_1ph0.getVal();

  m_sigma_1ph[pxID -1] = sigma_1ph.getVal();
  m_err_sigma_1ph[pxID -1] = sigma_1ph.getPropagatedError(*fitres);
  m_dsigma_1ph[pxID -1] = sigma_1ph.getVal() - sigma_1ph0.getVal();

  m_mean_1ct[pxID -1] = mean_1ct.getVal();
  m_err_mean_1ct[pxID -1] = mean_1ct.getPropagatedError(*fitres);
  m_dmean_1ct[pxID -1] = mean_1ct.getVal() - mean_1ct0.getVal();

  m_sigma_1ct[pxID -1] = sigma_1ct.getVal();
  m_err_sigma_1ct[pxID -1] = sigma_1ct.getPropagatedError(*fitres);
  m_dsigma_1ct[pxID -1] = sigma_1ct.getVal() - sigma_1ct0.getVal();
  
  m_Pmiss[pxID -1] = Pmiss.getVal();
  m_err_Pmiss[pxID -1] = Pmiss.getPropagatedError(*fitres);
  m_dPmiss[pxID -1] = Pmiss.getVal() - Pmiss0.getVal();

  m_pv_ratio[pxID -1] = pv_ratio;
  m_sig_peak[pxID -1] = x_sig_peak;
  m_mean_sig[pxID -1] = signal_pdf[0]->mean(x)->getVal() - mean_n.getVal();
  
  m_outFile->cd();
  fitres->Write();
  frame->Write();
}



